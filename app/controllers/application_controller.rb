class ApplicationController < ActionController::Base
  protect_from_forgery

  
 def authenticate 
   authenticate_or_request_with_http_token do |token, options|  
     @user = User.find_by_token(token)
   end
   return false unless @user
   @hotel = Hotel.find_by_id(params[:hotel_uuid])
   return false unless @hotel
   return false unless @hotel.id == @user.hotel_id
   return true
 end
 
    
end   
 


