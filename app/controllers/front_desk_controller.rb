class FrontDeskController < ApplicationController
  
  layout 'front_desk'
  
  def index
    
  end
  
  def login
     user = User.find_by_username(params[:username].downcase)
    
     if user.nil?
         render json: {success: false, messages: [type:"fail", message: "Invalid login and password combination entered"]}
         return 
     end
     
     if user.valid_password?(params[:password])
         token = user.generate_token
         hotel = user.hotel
         render json: {success: true, hotel: hotel.details, user: user.details, token: token}
         return         
     end
     
     render json: {success: false, messages: [type:"fail", message: "Invalid login and password combination entered"]}
  end
  
end
