class V1::RoomsController < V1::RequestController
  def create
    self.model = CreateRoom
    perform
  end

  def update    
    item = @hotel.find_room(params[:id]) or not_found
    self.model = UpdateRoom
    perform(item)
  end
  
  def destroy
    item = @hotel.find_room(params[:id]) or not_found
    self.model = DestroyRoom
    perform(item)    
  end  
end
