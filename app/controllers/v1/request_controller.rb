class V1::RequestController < ApplicationController  
  attr_accessor :model, :reservation, :hold
  before_filter :authenticate

  def version
    "1.0"
  end
  
  private
  
  def perform(item = nil)
     request = self.model.new(params[:request], @hotel)
     request.user = @user
     request.reservation = self.reservation
     request.hold = self.hold
     request.item = item
     begin     
        result = request.process  
     rescue => e
        result = exception_response(e)
     end        
     
     request.success = result[:success]
     request.message = request.success ? result[:message] : result[:errors]
     request.params = params
     request.version = self.version
     request.save
     render json: result.to_json   
  end  
  
  def exception_response(e)
     return {success: false, errors: [{text: I18n.t("api.exception"), code: "exception", debug: {message: e.message, trace: e.backtrace}}]}
  end
  
  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end
  

  
end
