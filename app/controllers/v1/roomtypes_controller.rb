class V1::RoomtypesController < V1::RequestController
  
  def create
    self.model = CreateRoomtype
    perform
  end
  
  def update    
    item = @hotel.find_roomtype(params[:id]) or not_found
    self.model = UpdateRoomtype
    perform(item)
  end
  
  def destroy
    item = @hotel.find_roomtype(params[:id]) or not_found
    self.model = DestroyRoomtype
    perform(item)    
  end
  
end
