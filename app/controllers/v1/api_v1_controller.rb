class ApiV1Controller < ApplicationController
 
  before_filter :authenticate

 
  def guest
     render text: params[:id]
  end
  
  def reservation

  end
  
  def hold
    
  end
  
  def availability
     
     params[:start_date].nil? ? start_date = Date.today.to_s : start_date = params[:start_date]
     
     params[:roomtypes].nil? ? roomtypes = [] : roomtypes = params[:roomtypes]
     roomtypes = @hotel.roomtypes.pluck(:uuid) if roomtypes && roomtypes[0] == 'all'
     
     params[:rooms].nil? ? rooms = [] : rooms = params[:rooms]    
     rooms = @hotel.rooms.pluck(:uuid) if rooms && rooms[0] == ('all')
     
     ids = roomtypes.concat(rooms)

     render json: @hotel.get_availability_from_couch(ids,start_date,params[:end_date]).to_json
  end
  
  def rates
     params[:start_date].nil? ? start_date = Date.today.to_s : start_date = params[:start_date]
     
     params[:roomtypes].nil? ? roomtypes = [] : roomtypes = params[:roomtypes]
     roomtypes = @hotel.roomtypes.pluck(:uuid) if roomtypes && roomtypes[0] == 'all'  
     
     render json: @hotel.get_rates_from_couch(roomtypes, start_date, params[:end_date]).to_json
     
  end
  
  private
  

  
end