class V1::SessionsController < ApplicationController

  def login
     result = User.login(params[:username].downcase, params[:password])
         
     unless result
         render json: {success: false, error: {code: "invalid_login", message: "Invalid login and password combination entered"}}
         return 
     end
     
     response = {}

     response[:user] = User.find_by_username(result[:name]).login_details 
     
     if params[:include_hotel]
       response[:hotel] = Hotel.find_by_id(response[:user][:hotel_id]).details
     end
     
     response[:token] = result[:cookie]
     render json: {success: true, message: response}
  end  
end