class V1::ReservationsController < V1::RequestController
  
  def create
     self.model = CreateReservation
     perform
  end
  
  def destroy
     self.model = DestroyReservation
     self.reservation = @hotel.find_reservation(params[:id])
     perform
  end
  
  def finalize
     self.reservation = @hotel.find_reservation(params[:id])
     self.model = FinalizeReservation
     perform    
  end
  
  def update
     self.reservation = @hotel.find_reservation(params[:id])
     self.model = UpdateReservation
     perform
  end
  
  def show
    url = @hotel.couch_url + params[:id]
    
    begin
    doc = RestClient.get(url)
    rescue # Rescue from the 404 error if the reservation isn't in couch      
      @reservation = @hotel.find_reservation(params[:id]) # Check to see if the reservation is in the SOR
      render json: @reservation.details.to_json if @reservation  
      return nil if @reservation  # need to return to stop the second render from being called           
      render json: ErrorCode.generate_response(2).to_json
      return nil
    end      
    
    render json: doc
  
  end
  
  private
  
  
end
