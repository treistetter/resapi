class HotelsController < ApplicationController

    def addaddress
      
      @hotel = current_user.hotel
      locationpath =  params[:hotel][:addaddress]
      params[:hotel].delete :addaddress
       if @hotel.update_attributes(params[:hotel])
         redirect_to locationpath     
       else
         render text: "something went wrong"
       end
    end   
    
    def booking
      params[:hotel_id].nil? ? @hotel = Hotel.where("domain = ?" , request.domain).first : @hotel = Hotel.find(params[:hotel_id])
    end
    
    def create
    @hotel = current_user.build_hotel(params[:hotel])
    if @hotel.save     
      flash[:success] = "Hotel successfully created!"
      redirect_to '/dashboard/new'
    else
      flash[:error] = "Hotel creation failed."
      redirect_to root_path
    end
  end
   def update
    @hotel = current_user.hotel
   respond_to do |format|
    if @hotel.update_attributes(params[:hotel])
      format.html{
      flash[:success] = "Hotel successfully updated"
      redirect_to '/dashboard'
      }
      format.json { respond_with_bip(@hotel) }
    else
      flash[:error] = "Something went wrong and the edit failed"
      redirect_to root_path
    end
    end
  end


  def serve
    params[:hotel_id].nil? ? @hotel = Hotel.where("domain = ?" , request.domain). first : @hotel = Hotel.find(params[:hotel_id]) 
    params[:hotel_id].nil? ? @hotelrootpath = "/" : @hotelrootpath = "/hotels/" + @hotel.id.to_s + "/"
    params[:page].nil? ? @page = @hotel.pages.where("path = ?", "").first : @page = @hotel.pages.where("path = ? ", params[:page]).first   
    @rooms = @hotel.rooms.all
    @page.pagetype ||= 0
    @navbar = @hotel.pages.where("navbar > ?", 0).order("navbar asc")
    @path = params[:page]
    @page_new = @hotel.pages.build
    @parents = @hotel.pages.where("navbar > ?" , 0)
    @pagetypes = Pagetype.order("id asc")
    if is_owner?
    @roomtype_new = @hotel.roomtypes.build()
    @roomtype = current_user.hotel.roomtypes.all
    @room_new = current_user.hotel.rooms.build
    @room = current_user.hotel.rooms.all
    # @roomtype.empty? ? @rate_edit = [] : @rate_edit = create_rates_edit(current_user.hotel, current_user.hotel.roomtypes.first, 1)
    end
  end
  

    
def format_date(date)
  date = date.to_s.split('-') #split date into array of strings
  date[0] = (date[0].to_i-2000).to_s #make year 2 digits
  date = [date[1], date[2], date[0]] #rearrange dates to MM/DD/YY
  date = date.join('/') #join backinto a string / divider
  
end    

 
end
