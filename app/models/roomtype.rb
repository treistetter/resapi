class Roomtype < CouchModel
  
  attr_accessor :name, :max_guests, :active, :description, :rates
  
  validates :name, presence: true, length: { maximum: 30 }
  validate  :unique_name
  validates :max_guests, presence: true, :numericality => {:only_integer => true}, :inclusion => 1..10
  
 
 
 def initialize(params, hotel = nil)
   super(params)
   self.max_guests = params[:max_guests].to_i
   
   self.generate_id unless self.id
   self.hotel = hotel
   self.db = self.hotel.db if hotel
 end
 
 def details
    {      name: self.name,
     max_guests: self.max_guests,
             id: self.id,
    description: self.description,
          rates: self.rates_details,
           type: "roomtype"
      }
 end
 
 def rates
   return @rates if @rates
   doc = self.db.get(self.id + "-rates")
   return @rates = [] unless doc
   return @rates = [] unless doc[:rates]
   @rates = []
   doc[:rates].each do |rate|
     @rates.push(Rate.new(rate))
   end
   return @rates  
 end
 
 def rates_details
   response = []
   self.rates.each do |rate|
     response << rate.details
   end
   return response
 end
 
 
 def save
   return self.create_new unless self.hotel.find_roomtype(self.id)
   if self.valid?
     return self.hotel.update_roomtype(self.details)
   else
     return false
   end
 end
 
 def destroy
   self.hotel.destroy_roomtype(self.id)
 end

 
 def create_new
   self.hotel.create_roomtype(self.details)
 end
 
 def save!
   raise "Roomtype failed to save" unless self.save
   return true
 end
 
 def create_document  
    design = self.hotel.design 

    views = design[:views]
    if views.nil?
      views = {}
    end
    
    return super if views["#{self.id}-availability".to_sym]
    
    views[self.id + "-availability"] = {}
    views[self.id + "-availability"]["map"] = self.map_function
    views[self.id + "-availability"]["reduce"] = self.reduce_function 
                    
    design[:views] = views
    
    self.hotel.update_design(design)
    
    super
    
  end


  def prefix
    return "rtp"
  end
  
  ## Validations ##
  
  def unique_name
    return true unless self.hotel
    self.hotel.roomtypes.each do |rt|
      if rt.name == self.name && rt.id != self.id
        errors.add(:name, I18n.t("api.duplicate", type: "Roomtype", field: "name", value: self.name)) 
        return false
      end
    end
  end


end
