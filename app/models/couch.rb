class Couch
  
  attr_accessor :url
  
  @@token = nil
  
  def initialize(url)
     self.set_database(url)
  end
  
  def handle_exception(e, rerun = false)
    
    raise e unless rerun
    
    return nil if e.class == RestClient::ResourceNotFound
  
    if e.class == RestClient::Unauthorized
      Couch.reset_token
      self.set_database(self.url)
      return true
    end

    if e.class == RestClient::Forbidden
      Couch.reset_token
      self.set_database(self.url)
      return true
    end
    
    raise e
  end
  
  def get(url, options = {}, rerun = true)
      return JSON.parse(self.database[url].get(options), symbolize_names: true)   
      rescue => e
        result = handle_exception(e, rerun)
        return nil unless result
        self.get(url, options, false)
  end
  
  def put(url, data = {}, rerun = true)    
      JSON.parse(self.database[url].put(data.to_json), symbolize_names: true)      
      rescue => e
        result = handle_exception(e, rerun)
        return nil unless result
        self.put(url, data, false)
  end
  
  def post(url, data = {}, rerun) 
      JSON.parse(self.database[url].post(data.to_json), symbolize_names: true)   
      rescue => e
        result = handle_exception(e, rerun)
        return nil unless result
        self.post(url, data, false)
  end
  
  def delete(url, rev, rerun = true)   
      Rails.logger.info("url is #{url}")
      Rails.logger.info("rev is #{rev}")
      url = url + "?rev=" + rev
      JSON.parse(self.database[url].delete, symbolize_names: true)
      rescue => e
        result = handle_exception(e, rerun)
        return nil unless result
        self.delete(url, false)    
  end
  
  def get_view_key(view, key, include_docs = true, rerun = true)
      url = self.view_url(view, include_docs) + "&key=" + CGI.escape(key.to_json)     
      result = JSON.parse(self.database[url].get, symbolize_names: true)
      if result[:rows][0]
        return result[:rows][0][:doc]
      else
        return nil  
      end
      rescue => e
        result = handle_exception(e, rerun)
        return nil unless result
        self.get_view_key(view, key, include_docs, false)
  end
  
  def get_view_range(view, start_key, end_key, include_docs = true)
    
  end
  
  def get_view(view, params = {}, rerun = true)
     url = self.view_url(view)    
    
     if params[:include_docs] == false
        url = url + "?include_docs=false"
     else
        url = url + "?include_docs=true"         
     end
     
     params.each do |k, v|
        url = url + "&" + k.to_s + "=" +  CGI.escape(v.to_json)       
     end  

     result = JSON.parse(self.database[url].get, symbolize_names: true)
     if result[:rows]
       return result[:rows]
     else
       return []  
     end    

     rescue => e
        result = handle_exception(e, rerun)
        return nil unless result
        self.get_view(view, params, false)
  end
  
  ## URLs ##
  
  def view_url(view)
    "_design/design/_view/#{view}"
  end
  
  
  ## Getters/Setters ##
  
  def set_database(url)
    self.url = url
    headers = {
        :accept => :json,
        :content_type => :json,
    }

    cookies = {}
    cookies["AuthSession"] = self.token
    @database = RestClient::Resource.new(url, :headers => headers, :cookies => cookies)
  end
  
  def database
    @database
  end

  
  def token
    return @@token if @@token
    @@token = Couch.reset_token
    return @@token
  end
  
  def self.token= (token)
    @@token = token
  end
  
  def self.reset_token
    response = RestClient.post 'http://127.0.0.1:5984/_session', "name=#{Rails.application.secrets.couchdb_user}&password=#{Rails.application.secrets.couchdb_password}",{:content_type => 'application/x-www-form-urlencoded'}
    @@token = response.cookies["AuthSession"]
    return @@token    
  end
  
  
end