class Address
  include ActiveModel::Model
  attr_accessor :city, :country, :line1, :line2, :region, :postal_code

  
  def create_document
    # stub so we don't save addresses
  end
  
  def generate_id
    # stud so we don't generate ID
  end
  
  def details
     {    line1: self.line1,
          line2: self.line2,
    postal_code: self.postal_code,        
        country: self.country,
           city: self.city,
         region: self.region
       }
  end
  
  def billing_format
    {address1: self.line1,
     address2: self.line2,
          zip: self.postal_code,
      country: self.country,
         city: self.city,
        state: self.region     
    }
  end
  
end
