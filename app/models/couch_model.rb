class CouchModel
  include ActiveModel::Model
  attr_accessor :_id, :_rev, :id, :db, :hotel, :type, :created_at, :updated_at
  
  def initialize(params, hotel = nil)
    super(params)
    self.hotel = hotel
    self.db = self.hotel.db if hotel
    self.generate_id unless self.id
  end  
  
  
  def save   
    return false unless self.valid?
  #  begin
      self.created_at = Time.now unless self.created_at
      self.updated_at = Time.now
      result = self.create_document
      if result[:ok]
        self._rev = result[:rev]
        return self
      else
        return false
      end
   # rescue
   #   return false
   # end  
  end
  
  def save!
    result = self.save
    raise "Failed to save: #{self.errors.messages}" unless result
    return result
  end
  
  def self.create(params, hotel = nil)
    object = self.new(params, hotel)
    return object.save
  end
  
  def self.create!(params, hotel = nil)
    result = self.create(params, hotel)
    raise "Failed to create" unless result
    return result
  end
  
  def destroy
    self.hotel.db.delete(self.id, self._rev)
  end
  
  def update_attributes(params)
    params.each do |key , value|
       self.send("#{key}=", value)
    end
    self.save!  
  end
  
  def stage_update(update)
    return false unless self.validate_fields(update)
    update.each do |key, value|
       method = key.to_s + "="
       self.send(method, value)
    end
    return true  
  end
  
  def validate_fields(fields)
    return true
  end
  
  
  def generate_id     
     begin
       self.id = self.prefix + SecureRandom.hex(4)
     end while self.class.find_by_id(self.id, self.hotel)
  end
  

  
  def self.all(limit = 100)
    
  end
  

  ## Finders ##
  
  def get_document
    self.db.get(self.id)
  end
  
  def self.find_by_id(id, hotel = nil)
    return nil unless hotel
    begin
      result = hotel.db.get(id)
      return result
    rescue
      return nil
    end
  end
  
  ## Getters/setters ## 
  
  def is_valid?
    true
  end
  
  def messages
    return @messages if @messages
    @messages = []
    return @messages    
  end
  
  def prefix
    return ""
    #stub overwritten in models
  end
  
  ## URLs ## 
  
 def couch_base
   Rails.application.secrets.couchdb
 end

 def self.couch_base
   Rails.application.secrets.couchdb
 end
  
  ## Creaters
  
  def create_document
    details = self.details
    details[:_rev] = self._rev if self._rev
    result = self.db.put(self.id, details)   
    self._rev = result[:rev]
    return result
  end
  
  def details
    {}
  end
     
  ## Updaters
   
 
  
end