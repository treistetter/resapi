class Hotel < CouchModel
  attr_accessor :name, :email, :phone, :phone2, :reservations_phone, :domain, :outgoing_email, :deposit_rate, :booking, :cc_override, :cc_sandbox, :cc_authorize, :cancel_policy, :roomtypes, :rooms, :extras, :tax_rates

  validates :name, presence: true, length: {maximum: 100}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, format: { with: VALID_EMAIL_REGEX }  
  validates :phone, allow_blank: true, allow_nil: true, :numericality => { :only_integer => true }, length: {is: 10}
  validates :phone2, allow_blank: true, allow_nil: true, :numericality => { :only_integer => true }, length: {is: 10}

  
  @@db = Couch.new(self.couch_base)
  
  def initialize(params = {}, hotel = nil)
    super(params, nil)    
    self.generate_id unless self.id
    self.cc_override = SecureRandom.hex(4) unless params[:cc_override]
    self.db = Couch.new(self.couch_base + self.id)
    self.instantiate_arrays(params)
  end
  
  def instantiate_arrays(params)
    self.creatables.each do |relation|
      key = relation + "s"
      if params[key.to_sym]
        klass = relation.to_s.classify.constantize
        items = []
        params[key.to_sym].each do |item|
          items << klass.new(item, self)
        end
        self.send(relation.to_s + "s=", items)         
      else
        self.send(relation.to_s + "s=", [])        
      end  
    end
  end
  
  def details
    response = {  name: self.name,
                 email: self.email,
                 phone: self.phone,
    reservations_phone: self.phone2,
                    id: self.id,
         cancel_policy: self.cancel_policy,
           cc_override: self.cc_override,
            cc_sandbox: self.cc_sandbox,
          cc_authorize: self.cc_authorize,
          deposit_rate: self.deposit_rate,
                  type: "hotel"
      }
      
      self.relations.each do |relation|        
        response[relation] = self.send(relation.to_s + "_details")
      end
      return response
  end
  
  def login_details
    self.details
  end
 
  
  ## Magic Methods ##
  def magic_build(method, *arguments)
      type = method[6, method.length - 1]
      klass = type.classify.constantize
      puts arguments[0].inspect
      item = klass.new(arguments[0], self)
      return item
  end
  
  def magic_create(method, arguments, bang)
      type = method[7, method.length - 1]
      klass = type.classify.constantize
      item = klass.new(arguments[0], self)
      if item.valid?
        self.send(type + "s").push(item)
        if self.save
          return item
        else
          raise "#{klass} failed to create" if bang 
          return false         
        end
      else
        raise "#{klass} failed to create" if bang
        return item
      end    
  end
  
  def magic_details(method)
    type = method.to_s.chomp("_details")
    items = self.send(type)
    response = []
    items.each do |item|
      response << item.details
    end
    return response
  end
  
  def method_missing(method, *arguments, &block)
    bang = false
    if method[-1, 1] == "!"
      bang = true
      method = method.to_s.chomp("!")
    end
       
    if method[0,5] == "build" && self.creatables.include?(method[6, method.length - 1])      
      return self.magic_build(method, arguments)
    end    
        
    if method[0,6] == "create" && self.creatables.include?(method[7, method.length - 1])      
      return self.magic_create(method, arguments, bang)        
    end
    if method[-7..-1] == "details" && self.creatables.include?(method.to_s.chomp("s_details"))  
      return self.magic_details(method)        
    end
    
    if method[0,4] == "find"
      raise "ID passed to find method was nil" unless arguments[0]
      lookup = method[5, method.length] + "s" # Method is something like find_roomtype.  Slice off find_ and add 's' to get the correct array
      items = self.send(lookup)
      raise "find method did not return array" unless items.is_a? Array
      items.each do |item|
        return item if item.id == arguments[0]
      end
      return nil
    end
    
    if method[0,7] == "destroy"
      raise "ID passed to find method was nil" unless arguments[0]
      lookup = method[8, method.length] + "_details"
      items = self.send(lookup)
      raise "find method did not return array" unless items.is_a? Array
      items.each do |item|
        items.delete_at(i) if item[:id] == arguments[0]
        return self.save
      end
      raise "No item found with ID #{arguments[0]}"           
    end
    
    if method[0,6] == "update"
      raise "ID passed to find method was nil" unless arguments[0]
      lookup = method[7, method.length] + "s"
      items = self.send(lookup)
      raise "find method did not return array" unless items.is_a? Array
      items.each do |item|
        items.delete_at(i) if item.id == arguments[0]
        return self.save
      end
      raise "No item found with ID #{arguments[0]}"  
    end
    
    if self.relations.include?(method)
      response = []
      model = method.to_s.classify.constantize
      self.db.get_view(method).each do |result|
        response << model.new(result[:doc], self)
      end
      return response
    end
   
    super(method, arguments, block)
  end
  
  
  def relations
    [:roomtypes, :rooms, :tax_rates, :extras]
  end
  
  def creatables
    return @creatables if @creatables
    @creatables = ["roomtype", "room", "tax_rate", "extra"]
    return @creatables
  end
  
  ## Getters ## 
  
  def design
    self.db.get("_design/design")
  end
  
  def room_details
    response = []
    self.rooms.each do |room|
      response << room.hotel_details
    end
    return response
  end
  
  def extra_details
    response = []
    self.extras.each do |extra|
      response << extra.details
    end
    return response
  end
  
  def find_reservation(id)
    doc = self.db.get(id)
    if doc
      return Reservation.new(doc, self)
    else
      return nil
    end
  end

  def find_rate(id)
    return Rate.find_by_id(id, self)
  end
  
  def find_payment(id)
    doc = self.db.get(id)
    if doc
      return Payment.new(doc, self)
    else
      return nil
    end
  end  

  ## Updates ##
  
  def update_design(design)
    self.db.put("_design/design", design)
  end
  
  ## Override Couch Methods ##
  
  def prefix
    "hot"
  end
  
  def self.find_by_id(id, hotel = nil) 
      result = self.db.get("#{id}/#{id}")
      if result
        return Hotel.new(result)
      else
        return nil
      end
  end
  
  def self.db
    @@db
  end
  
  ## Initializers for new hotels
  
  def create_document
    
    return super if self._rev # We are doing an update so don't need to do below
    
    Hotel.db.put(self.id, '') # create the hotel DB        
    security = {admins: {names: [Rails.application.secrets.couchdb_user], roles: []}, members: {names: [], roles:[self.id]}}
    self.db.put("_security", security)
    
    views = {}    
    history_map = 'function(doc){if(doc.history == true){emit(doc._id, {reservation_id: doc.item_id, info: doc.info, type: doc.type, created_at: doc.created_at});}}'
    views["history"] = {}
    views["history"]["map"] = history_map
    reservation_history_map = 'function(doc){if(doc.type=="request" && doc.display == true){emit([doc.reservation, doc.epoch], {history: doc.history, type: doc.request_type, item_id: doc.item_id, item_type: doc.item_type});};if(doc.type=="payment"){emit([doc.reservation, doc.epoch],{history: doc.history, type: "payment", id: doc._id});};}'
    views["reservation_history"] = {}
    views["reservation_history"]["map"] = reservation_history_map  
    payments_map = 'function(doc){if(doc.type=="payment"){emit([doc.reservation, doc.epoch], doc);}}'
    views["payments"] = {}
    views["payments"]["map"] = payments_map  
    arriving_map = 'function(doc){if(doc.type=="reservation"){ var staysLength = doc.stays.length;for(var i=0;i<staysLength;i++){emit(doc.stays[i].check_in, {first_name: doc.guest.first_name, last_name: doc.guest.last_name, id: doc._id, room: doc.stays[i].room});}}}'
    views["arriving"] = {}
    views["arriving"]["map"] = arriving_map
    departing_map = 'function(doc){if(doc.type=="reservation"){ var staysLength = doc.stays.length;for(var i=0;i<staysLength;i++){emit(doc.stays[i].check_out, {first_name: doc.guest.first_name, last_name: doc.guest.last_name, id: doc._id, room: doc.stays[i].room});}}}'
    views["departing"] = {}
    views["departing"]["map"] = departing_map   
    start_map = 'function(doc){if(doc.type=="reservation"){emit(doc.start, null)}}'
    views["byStart"] = {}
    views["byStart"]["map"] = start_map    
    last_name_map = 'function(doc){if(doc.type=="reservation"){emit(doc.guest.last_name.toLowerCase(), null)}}'
    views["byLastName"] = {}
    views["byLastName"]["map"] = last_name_map      
    id_map = 'function(doc){if(doc.type=="reservation"){emit(doc._id, null)}}'
    views["byID"] = {}
    views["byID"]["map"] = id_map    
    phone_map = 'function(doc){
    if(doc.type=="reservation"){emit(doc.guest.phone, null);if(doc.guest.phone_secondary != null){emit(doc.guest.phone_secondary, null);};};}'
    views["byPhone"] = {}
    views["byPhone"]["map"] = phone_map  
    email_map = 'function(doc){if(doc.type=="reservation"){emit(doc.guest.email, null)}}'
    views["byEmail"] = {}
    views["byEmail"]["map"] = email_map 
    rooms_map = 'function(doc){if(doc.type=="hotel"){for(var i=0;i<doc.rooms.length;i++){emit([doc.rooms[i].roomtype_id], doc.rooms[i])}}}'
    views["rooms"] = {}
    views["rooms"]["map"] = rooms_map
    extras_map = 'function(doc){if(doc.type=="extra"){emit(doc.id, null)}}'
    views["extras"] = {}
    views["extras"]["map"] = extras_map    
    rates_map = 'function(doc){if(doc.type=="hotel"){for(var i=0;i<doc.roomtypes.length;i++){for(var j=0;j<doc.roomtypes[i].rates.length;j++){emit([doc.roomtypes[i].id, doc.roomtypes[i].rates[j].date], doc.roomtypes[i].rates[j])}}}}'
    views["rates"] = {}
    views["rates"]["map"] = rates_map
    rates_by_id_map = 'function(doc){if(doc.type=="hotel"){for(var i=0;i<doc.roomtypes.length;i++){for(var j=0;j<doc.roomtypes[i].rates.length;j++){emit(doc.roomtypes[i].rates[j].id, doc.roomtypes[i].rates[j])}}}}'
    views["rates_by_id"] = {}
    views["rates_by_id"]["map"] = rates_by_id_map
    occupancies_map = 'function(doc){if(doc.type=="reservation" && doc.cancelled != true && doc.finalized == true){for(var i=0;i<doc.occupancies.length;i++){var occ = JSON.parse(JSON.stringify(doc.occupancies[i]));occ.reservation_id = doc.id;emit([occ.room_id, occ.date], occ)}}}'
    views["occupancies"] = {}
    views["occupancies"]["map"] = occupancies_map
    
    validate = 'function(new_doc, old_doc, userCtx){if(userCtx.roles[0] != "_admin"){throw({forbidden: "Only server may update documents"});}}'        

    self.db.put("/_design/design", {views: views, validate_doc_update: validate}) # create design document
    self.db.put(self.id, self.details) # put the hotel's information 
  end 
     
end  