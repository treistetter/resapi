class Occupancy < CouchModel

  attr_accessor :extras_array, :date, :rate, :room_id, :guests, :extra_ids, :hold_id,  :black_out, :fixed_room, :cancelled, :rate_id

  
  validates :date, presence: true 
  validates :room_id, presence: true 
  validates :guests, presence: true, numericality: {only_integer: true}, inclusion: 1..11
  validate  :valid_date? 
  validate  :valid_room
  validate  :unique_expired_holds?, if: :valid_date? # check to see if the hold is unique excepting expired holds
  validate  :below_max_guests
  
   def initialize(params = {}, hotel = nil) 
     
     self.room_id = params[:room_id]
     self.room_id = params[:room] if params[:room_id].nil? && params[:room].is_a?(String) 
     self.room_id = params[:room].id if params[:room].is_a?(Room)
        
     self.extra_ids = params[:extra_ids] if params[:extra_ids] && params[:extra_ids][0].is_a?(String)
     self.extra_ids = [] unless self.extra_ids

     if params[:date].is_a?(Date)
       self.date = params[:date]
     elsif params[:date].is_a?(String)
       self.date = Date.parse(params[:date])
     else
       self.date = params[:date]  
     end
     super_params = params.clone
     super_params.delete(:room_id)
     super_params.delete(:extras)
     super_params.delete(:extra_ids)
     super_params.delete(:date)
     super(super_params, hotel)
     
     if self.rate_id
       self.rate = self.hotel.find_rate(self.rate_id)
     else
       room = self.hotel.find_room(self.room_id)
       self.rate = Rate.resolve(room.roomtype.id, self.date, self.hotel) if room     
     end
   end
  
   def expired?
       Time.now.utc > self.hold_expiration
   end 
   
   def details
      response = {date: self.date.to_s, 
               room_id: self.room_id,  
                guests: self.guests, 
             extra_ids: self.extra_ids,
                    id: self.id
       }
       
       response[:rate_id] = self.rate.id if self.rate
       return response
   end
   
   def calculate_cost
       response = self.rate.total
       response[:extras] = Extra.total_extras(self.extras) 
       response[:total_cents] += response[:extras][:total_cents]
       response[:total] = Money.new(response[:total_cents]).format
       response[:rate_cents] = self.rate.rate.cents
       response[:rate] = self.rate.rate.format
       response[:gross_cents] = response[:rate_cents] + response[:extras][:gross_cents]
       response[:gross] = Money.new(response[:gross_cents]).format
       response[:date] = self.date.to_s
       response[:room] = self.room_id
       response[:id] = self.id
       response[:guests] = self.guests
       return response
   end
   
   def rate
     return @rate if @rate
     if self.rate_id
       @rate = self.hotel.find_rate(self.rate_id)
     else
       self.resolve_rate
     end
   end
   
   def room
     return @room if @room
     return @room = nil unless self.room_id
     return @room = nil unless self.hotel
     @room = self.hotel.find_room(self.room_id)
     return @room
   end
   
   def extras
     return @extras unless @extras.nil?
     @extras = []
     
     self.extra_ids.each do |extra_id|
       @extras << self.hotel.find_extra(extra_id)
     end
     return @extras
   end
   
   def extras_details
      response = []
      self.extras.each do |extra|
        response << extra.details
      end
      return response
   end
   
   def resolve_rate
     return @rate_obj = nil unless self.room && self.date && self.hotel
     @rate_obj = Rate.resolve(self.room.roomtype_id, self.date, self.hotel)
   end
   
   def self.total_occupancies(occupancies)
      response = {}
      response[:total_taxes_cents] = 0
      response[:total_cents] = 0
      response[:gross_rate_cents] = 0 
      response[:gross_extras_cents] = 0
      response[:occupancies] = []
           
      occupancies.each do |occ|
         occ_cost = occ.calculate_cost
         response[:total_taxes_cents] += occ_cost[:taxes][:total_cents] + occ_cost[:extras][:total_taxes_cents]
         response[:total_cents] += occ_cost[:total_cents]  
         response[:gross_extras_cents] += occ_cost[:extras][:gross_cents]
         response[:gross_rate_cents] += occ_cost[:rate_cents]
         response[:id] = occ.id
         response[:occupancies] << occ_cost
      end
      response[:gross_cents] = response[:gross_rate_cents] + response[:gross_extras_cents]
      response[:gross] = Money.new(response[:gross_cents]).format
      response[:gross_rate] = Money.new(response[:gross_rate_cents]).format
      response[:gross_extras] = Money.new(response[:gross_extras_cents]).format
      response[:total_taxes] = Money.new(response[:total_taxes_cents]).format
      response[:total] = Money.new(response[:total_cents]).format
      return response          
   end
      
   ## Validations ## 
   
   def valid_date?    
     unless self.date
        errors.add(:date, "Must have a valid date")
        return false
     end
     return true
   end
   
       
   def today_or_later?
     return false unless self.valid_date?
     unless self.date >= Date.yesterday
      errors.add(:date,'Must be today or later')
      return false
     end
     return true
   end
   
   def valid_room
     errors.add(:room_id, "#{self.room_id} was not found") unless self.room
   end
   
   def below_max_guests
      return true unless self.room 
      rt = hotel.find_roomtype(self.room.roomtype_id)
      return true unless rt
      return true unless self.guests
      errors.add(:guests, "Guests must be fewer than max guests of roomtype") if self.guests > rt.max_guests
   end
   
   def unique_expired_holds?
       return @unique_expired unless @unique_expired.nil?
       
       params = {key: [self.room_id, self.date.to_s]}
       contenders = self.hotel.db.get_view("occupancies", params)
       if contenders.empty?
         @unique_expired = true
         return true
       end
       
       name = self.room_id
       date = self.date.to_formatted_s(:long)
       
       contenders.each do |occ|
         occ[:value].delete(:reservation_id) ## The reservation_id is added to the view for the front desk.  We need to delete it since it isn't part of hte occupancy model
         occ_obj = Occupancy.new(occ[:value], self.hotel)
         if occ_obj.id == self.id ## This occupancy has already been saved so we know it's valid    
            @unique_expired = true
            return true
         end

         if occ_obj # This occupancy belongs to a reservation has not been cancelled.  CouchDB view skips cancelled occupancies
            errors.add(:room_id, "#{name} has already been reserved on #{date}.")
         else
            time_remaining = Time.parse(occ_obj.hold_expiration) - Time.now
            next if time_remaining < 0 # This occupancy is a hold that has expired
            errors.add(:room_id, "#{name} has already been held on #{date}.  Hold expires in #{time_remaining} seconds")
            @unique_expired = false            
         end   
       end
       
       @unique_expired = true
       return true
   end
   

   
   def prefix
     return "occ"
   end
  
end
