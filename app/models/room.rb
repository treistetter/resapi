class Room < CouchModel
  
  attr_accessor :name, :roomtype_id
  
  validates :name, presence: true, length: { maximum: 30 }
  validates :roomtype_id, presence: true
  validate :valid_roomtype
  
  def details
   return { id: self.id, 
   roomtype_id: self.roomtype_id,
          name: self.name, 
          type: "room" }
  end
  
  def hotel_details
    response = self.details
    response[:max_guests] = self.roomtype.max_guests if self.roomtype
    return response
  end
  
  def initialize(params, hotel = nil)
    super(params)
    self.generate_id unless self.id
    self.hotel = hotel
    self.db = self.hotel.db if hotel
  end
  
  def roomtype
    return @roomtype if @roomtype
    @roomtype = self.hotel.find_roomtype(self.roomtype_id)
    return @roomtype
  end
  
  def destroy
    self.hotel.destroy_room(self.id)
  end
    
  def create_document    
    design = self.hotel.design 
    views = design[:views]
    if views.nil?
      views = {}
    end
    
    return super if views["#{self.id}-availability".to_sym]    
    
     ## Create the room view       
    views[self.id + '-availability'] = {}
    views[self.id + '-availability']["map"] = "function(doc){if(doc.room=='" + self.id + "'){emit(doc.date,doc.available);}}"  
    design[:views] = views
    self.hotel.update_design(design)
    super
  end 

  def valid_roomtype
    return true unless self.hotel
    rt = self.hotel.find_roomtype(self.roomtype_id)   
    unless rt        
        errors.add(:roomtype, "Roomtype #{self.roomtype_id} is not valid") 
        return false      
    end
  end
  
  def unique_name
    return true unless self.hotel
    self.hotel.rooms.each do |rm|
      if rm.name == self.name
        errors.add(:name, I18n.t("api.duplicate", type: "Room", field: "name", value: self.name)) 
        return false
      end
    end
  end
    
  def prefix
    return "rmm"
  end
  
end