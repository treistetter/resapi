class DestroyRoom < DestroyRequest
  
  def description
    "Room"
  end
  
  def info 
    "Room #{self.item.name} successfully deleted"
  end
  
  def history_info
    "Room #{self.item.name} deleted by #{self.user.name}"
  end
  
end