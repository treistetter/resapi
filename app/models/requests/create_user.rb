class CreateUser < CreateRequest
 
  def model
    User
  end
  
  def description
    "User"
  end
  
  def received_item
    return nil unless @request_object 
    @request_object[:new_user].nil? ? {} : @request_object[:new_user] 
  end
  
end