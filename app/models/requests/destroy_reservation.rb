class DestroyReservation < ReservationRequest
    def process
       return self.reservation_not_found_response unless self.reservation      
       return self.failed_response("Cancel Reservation") unless self.cancel_reservation
       return self.valid_response
    end
    
    ## Responses
    
    def valid_response
      return {success: true, message: "Reservation #{self.reservation.uuid} successfully cancelled.", cancel_confirmation: self.reservation.cancel_confirmation}
    end
    
    ## Process ##
    
    def cancel_reservation
      begin
       self.reservation.cancel(self.user, self.received_cancel_reason)
      rescue
        return false  
      end       
      return true
    end
    
    ## Getters
    
    def received_cancel_reason
      received = @request_object ? @request_object[:reason] : nil        
    end
    
    def details
      top = super
      top[:item_id] = self.reservation.id
    end    
    
    ## Update Couch ##
    
    def update_couch
      return nil unless self.success
      self.create_document         
      self.reservation.delete_from_couch
    end    

    ## Code for features not currently used 
    
    def token_reservation_id_mismatch
      return {success: false, message: "The received cancel token did not match that stored in the reservation"}
    end
    
    def charge_due_on_cancel
        return {success: true} if @reservation.due_on_cancel == 0
        return {success: true} if self.received_cc_override == @hotel.cc_override
        return {success: false, message: "The received payment override did not match what was stored"} if self.received_cc_override && self.received_cc_override != @hotel.cc_override  
        return {success: false, message:"The received due on cancel does not match what was calculated"} unless (self.received_due_on_cancel * 100).round(0) == @reservation.due_on_cancel
        return {success: false, message:"Received both a payment profile and card"} if self.received_payment_profile && self.received_card
        return {success: false, message:"There is a due on cancel balance and no payment information was supplied"} unless self.received_payment_profile || self.received_card
        return self.charge_stored_card if self.received_payment_profile
        return self.charge_card if self.received_card
        return false       
     end
    
    def charge_stored_card
       amount = @reservation.due_on_cancel
       payment = @reservation.payments.new(amount_cents: amount)
       return payment.process_stored_card(payment_profile_id: self.received_payment_profile, method: :auth_capture)
    end
    
    def charge_card
       amount = @reservation.due_on_cancel
       payment = @reservation.payments.new(amount_cents: amount)
       return payment.authorize_and_capture(card: self.received_card, billing_address: self.received_billing_address)
    end
    
    def cancel_token_reservation_match?
      return @reservation.cancel_token == self.received_cancel_token
    end  
end