class CreateRoomtype < CreateRequest
  
  def model
    Roomtype
  end
  
  def description
    "Roomtype"
  end
  
  def received_item
    return nil unless @request_object 
    @request_object[:new_roomtype].nil? ? {} : @request_object[:new_roomtype] 
  end
  

end