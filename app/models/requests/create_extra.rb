class CreateExtra < CreateRequest
  def model
    Extra
  end
  
  def description
    "Extra"
  end
  
  def received_item
    return nil unless @request_object 
    @request_object[:new_extra].nil? ? {} : @request_object[:new_extra] 
  end
end