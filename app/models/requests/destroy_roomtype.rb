class DestroyRoomtype < DestroyRequest
  
  def description
    "Roomtype"
  end
  
  def info 
    "Roomtype #{self.item.name} successfully deleted"
  end
  
  def history_info
    "Roomtype #{self.item.name} deleted by #{self.user.name}"
  end
  
end