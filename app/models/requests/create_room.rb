class CreateRoom < CreateRequest
  def model
    Room
  end
  
  def description
    "Room"
  end
  
  def received_item
    return nil unless @request_object 
    @request_object[:new_room].nil? ? {} : @request_object[:new_room] 
  end
end