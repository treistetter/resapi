class Request < CouchModel
   
  attr_accessor :guest, :payment, :card, :request_object, :user, :hotel, :debug, :reservation, :success, :message, :hold, :item, :params, :version

  def initialize(request = nil, hotel = nil)
      self.hotel = hotel
      self.db = hotel.db if hotel
      return @request_object = nil unless request
      @request_object = request
      return @request_object unless @request_object.kind_of?(String)      
      @request_object = JSON.parse(self.request, symbolize_names: true)
      rescue JSON::ParserError
      @request_object = nil
  end
  
  ## Validations ##
  
  def validate_request
    unless @request_object
       self.debug.push({text: I18n.t("api.invalid_json"), code: "invalid_json"}) 
       return false
    end   
    return true
  end
  
  ## Responses ##
  
  def invalid_request_response    
     return {success: false, errors: [{text: I18n.t("api.invalid_request"), code: "invalid_request", debug: self.debug}]}
  end
  
  def failed_response(type)
     return {success: false, errors: [{text: I18n.t("api.unknown", type: type), code: "unknown"}]}
  end
  
  ## Getters ## 

  def details
      response = {       id: self.id,
          params: self.params,
            user: self.user.id,
         message: self.message,
            type: self.class.to_s,
         version: self.version,
            info: self.history_info,
         display: true,
         history: true     
      }
      response[:created_at] = self.created_at.nil? ? Time.now.to_i : self.created_at
      return response
  end
  
  def debug
    return @debug if @debug
    @debug = []
    return []
  end
    
  def hotel_id
    @request_object ? @request_object[:hotel_id] : nil
  end
  
  def messages
    return @messages unless @messages.nil?
    @messages = []
    return @messages
  end
  
  def id
    self.created_at.to_i.to_s + "-" + self.class.to_s + "-" + rand(1000).to_s # Add id in the off chance we get two of the same request types in the same second
  end

  def prefix
    "ch"
  end
  
  def history_info
    "Request #{self.id}"
  end
  
end
