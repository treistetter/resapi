class DestroyRequest < Request

   
  ## Handle request for single item ## 
 
  def process
    if self.item.destroy
       return self.valid_response
    else
       return self.failed_response(self.description)
    end    
  end
  
  def valid_response
     return {success: true, info: self.info}
  end
end