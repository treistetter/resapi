class CreateRequest < Request

  attr_accessor :new_item, :model, :description, :multiple 
   
  ## Handle request for single item ## 
 
  def process
    return self.invalid_request_response unless self.validate_request    
    return self.process_multiple if self.multiple # check if we have a request with multiple items
    if self.new_item.save
       return self.valid_response
    else
       return self.failed_response(self.description)
    end    
  end
  
  def valid_response
     return {success: true, message: self.new_item.details}
  end

  def validate_request
     super     
     return false unless @request_object
     return true if self.multiple
     self.new_item = self.model.new(self.received_item, self.hotel)
     return true if self.new_item.valid?
     self.debug = self.new_item.errors.to_hash(full_messages: true) 
     return false
  end
  
  def received_item
    {}
  end
  
  ## Handle request for multiple items ##
  
  def process_multiple
    return self.invalid_request_response unless self.validate_all
    return self.save_all
  end
  
  def validate_all
    self.new_items = []
    self.debug = []
    all_valid = true
    self.received_item.each do |item|
      new_item = self.model.new(item, self.hotel)
      all_valid = false unless new_item.valid?
      self.new_items << new_item
      self.debug << new_item.errors.to_hash(full_messages: true) 
    end
    return all_valid
  end
  
  def save_all
    self.new_items.each do |item|
      return self.failed_response unless item.save
    end
    return self.all_valid_response 
  end
  
  def all_valid_response
     details = []
     self.new_items.each do |item|
       details << item.details
     end
     return {success: true, message: details}     
  end
  


end