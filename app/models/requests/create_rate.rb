class CreateRate < CreateRequest
  
  def process
    
    result = super
    if result[:success]
      self.delete_
    else
      return result  
    end
  end
  
  def model
    Rate
  end
  
  def description
    "Rate"
  end
  
  def received_items
    return nil unless @request_object 
    item = @request_object[:new_rates].nil? ? {} : @request_object[:new_rates] 
    self.multiple = true if item.is_a?(Array)
    return item
  end
  
  def received_roomtype
    return nil unless @request_object
    return Roomtype.new(@request_object[:roomtype], hotel)
  end
  
end