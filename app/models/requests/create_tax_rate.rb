class CreateTaxRate < CreateRequest
  def model
    TaxRate
  end
  
  def description
    "Tax Rate"
  end
  
  def received_items
    return nil unless @request_object 
    item = @request_object[:new_tax_rate].nil? ? {} : @request_object[:new_tax_rate] 
    self.multiple = true if item.is_a?(Array)
    return item
  end
end