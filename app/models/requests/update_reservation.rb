class UpdateReservation < ReservationRequest
     
   attr_accessor :update_flag, :updated_occupancies, :created_extras, :invalid_delete_extras, :invalid_delete_occupancies
   
   def process
       result = self.process_update
       return result unless result == true
       return self.invalid_occupancies_response unless self.reservation.valid?  
       if self.reservation.save
         # send mail
         return self.valid_response
       else
         return self.failed_response("Update Reservation") unless self.update_reservation         
       end
   end
   
   ## Responses
   
 
  
   def valid_response
      response = {}
      response[:success] = true
      response[:message] = self.reservation.details   
      return response     
   end  
  

   ## Getters ##
   
   def received_occupancies
     # Map create_occupancis to occupancies to allow reuse of methods in reservation_request
     return self.received_create_occupancies
   end
   
   
    def details
      top = super
      top[:item_id] = self.reservation.id
      return top
    end

end