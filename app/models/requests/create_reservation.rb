class CreateReservation < ReservationRequest

    def process
      return self.invalid_request_response unless self.validate_request
      if self.reservation.save
        return self.valid_response
      else
        return self.failed_response(self.description)
      end    
    end

    
    ## Responses 
    
    def valid_response
      response = {}
      response[:success] = true
      response[:message] = self.reservation.details
      response[:message][:cancel_policy] = self.hotel.cancel_policy
      return response
    end
    

    
    ## Validations
    
    def validate_request
      super 
      return false unless @request_object
      self.reservation = Reservation.new({
                  guest: self.received_guest,
            occupancies: self.received_occupancies,
              extra_ids: self.received_extras,
              finalized: false,
              cancelled: false, 
        
      }, self.hotel)
      return true if self.reservation.valid?
      self.debug = self.reservation.errors.to_hash 
      return false
    end

   
    
    ## Getters
     
    def details
      top = super
      top[:item_id] = self.reservation.id
      return top
    end
end