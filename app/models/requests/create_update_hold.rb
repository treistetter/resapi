class CreateUpdateHold < ReservationRequest
           
   def process
       result = self.process_update
       return result unless result == true
       return self.valid_response 
   end
   
   
   def valid_response
      response = {}
      response[:message] = {}
      response[:success] = true
      response[:message][:new_invoice] = self.reservation.invoice
      return response
   end
   

   
   def received_occupancies # Override ReservationRequest method 
      self.received_create_occupancies
   end
   
   def received_create_occupancies
       @request_object ? @request_object[:create_occupancies] : nil
   end
   
   def received_delete_occupancies
       @request_object ? @request_object[:delete_occupancies] : nil
   end
       
   def received_reservation_id
       @request_object ? @request_object[:reservation_id] : nil
   end
end