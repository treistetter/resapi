class UpdateRoomtype < UpdateRequest
  
  def description
    "Roomtype"
  end
  
  def info
    "Roomtype #{self.item.name} successfully updated"
  end
  
  def history_info
    "Roomtype #{self.item.name} updated by #{self.user.name}"
  end
  
end