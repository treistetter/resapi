class ReservationRequest < Request
    attr_accessor  :saved_occupancies, :created_occupancies, :created_occupancies_errors, :hold_duration, :payment, :old_invoice, :deposit_result, :authorize_cc_result
    
    ## Responses ##
    
   def hold_not_found_response
      response = {}
      response[:success] = false
      response[:message] = "The hold corresponding to the supplied Hold ID was not found.  Was: #{self.received_hold_id}"
      return response
   end
   
   def reservation_not_found_response
      response = {}
      response[:success] = false
      response[:message] = "Reservation not found.  Was #{self.received_reservation_id}"
      return response
   end
   
   def invalid_occupancies_response
     
   end

   def invalid_delete_extras_response
      self.debug = {delete_extras: "Delete extras contained IDs not belonging to the reservation.  Were: #{self.reservation.invalid_delete_extras.to_s}"}
      return {success: false, error: {text: I18n.t("api.invalid_request"), code: "invalid_request", debug: self.debug}}
   end

   def invalid_delete_occupancies_response
      self.debug = {delete_occupancies: "Delete occupancies contained IDs not belonging to the reservation.  Were: #{self.reservation.invalid_delete_occupancies.to_s}"}
      return {success: false, error: {text: I18n.t("api.invalid_request"), code: "invalid_request", debug: {delete_occupancies: self.debug}}}
   end  
   
   def invalid_modify_occupancies_response
      return {success: false, error: {text: I18n.t("api.invalid_request"), code: "invalid_request", debug: {update_occupancies: self.reservation.modify_occupancy_errors}}}
   end
          
    ## Methods supporting process ##  
    
    def process_update    
       return self.invalid_request_response unless self.validate_request
       return self.invalid_delete_occupancies_response unless self.reservation.delete_occupancies(self.received_delete_occupancies)
       self.reservation.create_occupancies(self.received_create_occupancies)
       return self.invalid_delete_extras_response unless self.reservation.delete_extras(self.received_delete_extras)
       self.reservation.extra_ids = self.reservation.extra_ids + self.received_create_extras
       return self.invalid_modify_occupancies_response unless self.reservation.modify_occupancies(self.received_update_occupancies)
       return true
    end
         
    
    
   ## Deposit and Payment methods
   
   def charge_deposit        
     return {success: true} if self.received_cc_override == self.hotel.cc_override
     return self.deposit_result = {success: false, errors: [{code: "invalid_card", text: "Card is not valid"}], debug: self.card.errors.to_hash} unless self.card.valid?
     
     deposit = self.reservation.invoice_with_deposit[:deposit_due_cents]
     description = "Deposit at #{self.hotel.name} for reservation: #{self.reservation.id}"
     self.payment = Payment.new({amount_cents: deposit, description: description}, self.hotel)
     
     billing_address = self.reservation.guest.address
     order_id = self.payment.id
     params = {card: self.card, billing_address: billing_address, description: description, order_id: order_id}
     self.deposit_result = self.payment.authorize_and_capture(params)
     self.reservation.payments << self.payment
     unless self.deposit_result[:success]
        response = {}
        response[:success] = false
        response[:errors] = [{code: "payment_failed", text: self.deposit_result[:message], debug: self.deposit_result}]
        self.deposit_result = response
     end    
     return self.deposit_result
   end
   
   
   def authorize_cc
     return self.authorize_cc_result = {success: false, errors: [{code: "invalid_card", text: "Card is not valid", debug: self.card.errors.to_hash}]} unless self.card.valid?     
     description = "Card validation by #{self.hotel.name} for reservation: #{self.reservation.id}"
     self.payment = Payment.new({amount_cents: 1, description: description}, @hotel)
     params = {card: self.card, billing_address: self.reservation.guest.address, description: description}
     self.authorize_cc_result = self.payment.authorize(params) 
     self.reservation.payments << self.payment
     unless self.authorize_cc_result[:success]
        response = {}
        response[:success] = false
        response[:errors] = [{code: "payment_failed", text: self.authorize_cc_result[:message], debug: self.authorize_cc_result}]
        self.authorize_cc_result = response
     end     
     return self.authorize_cc_result
   end

   def store_cc
     auth_card = self.guest.build_payment_profile
     @store_cc_result = auth_card.create_profile(card: self.received_card)
   end
    
    ## Getters ## 
    
    def received_payment_profile
      @request_object ? @request_object[:payment_profile] : nil
    end
    
    def received_due_on_cancel
      @request_object ? @request_object[:due_on_cancel] : nil
    end
       
    def received_cancel_policy
       @request_object ? @request_object[:cancel_policy] : nil
    end
    
    def received_reservation_id
      @request_object ? @request_object[:reservation_id] : nil
    end
    
    def received_cancel_token
      @request_object ? @request_object[:cancel_token] : nil
    end
    
    def received_delete_extras
      received = @request_object ? @request_object[:delete_extras] : []  
      received = [] if received.nil?
      return received    
    end
   
    def received_create_extras
      received = @request_object ? @request_object[:create_extras] : []     
      received = [] if received.nil?
      return received   
    end
   
    def received_update_occupancies
      received = @request_object ? @request_object[:update_occupancies] : []      
      received = [] if received.nil?
      return received   
    end
   
    def received_delete_occupancies
      received = @request_object ? @request_object[:delete_occupancies] : []     
      received = [] if received.nil?
      return received   
    end   

   
    def received_create_occupancies
      received = @request_object ? @request_object[:create_occupancies] : []     
      received = [] if received.nil?
      return received   
    end   
    
    
    def received_card_with_name
      card = self.received_card
      return nil unless card
      guest = self.received_guest
      return nil unless guest
      card[:first_name] = guest[:first_name]
      card[:last_name] = guest[:last_name]
      return card
    end
    
    def card_required?
       return true if self.hotel.deposit_rate || self.hotel.cc_authorize && self.hotel.cc_override != self.received_cc_override
       return false
    end
    
    def received_cc_override
      @request_object ? @request_object[:cc_override] : nil
    end
    
    def received_card
       @request_object ? @request_object[:card] : nil
    end
    
    def total_reservation_cost
      return @total_reservation_cost
    end
    
    def received_billing_address 
       @request_object[:billing_address].inspect
       @request_object ? Address.new(@request_object[:billing_address]) : Address.new()
    end
    
    def received_hold_id
      @request_object ? @request_object[:hold_id] : nil
    end
    
    def received_hold_duration
      @request_object ? @request_object[:hold_duration] : nil      
    end
    
    def hold_occupancies
      return nil unless @hold
      @hold_occupancies = @hold.occupancies.all
      return @hold_occupancies
    end

    def received_occupancies 
       @request_object ? @request_object[:occupancies] : []
    end
    
    def received_extras
      return nil unless @request_object 
      @request_object[:extras].nil? ? [] : @request_object[:extras] 
    end
    
    def received_guest 
      @request_object ? @request_object[:guest] : {}
    end
    
    def hold_duration
      @request_object ? @request_object[:hold_duration].to_i : nil
    end
     
end