class TaxRate < CouchModel
  
  attr_accessor :rate, :description, :active, :name
  
  validates :rate, presence: true, numericality: {greater_than: 0, less_than: 1}
  validates :description, presence: true, length: {maximum: 1000}
  validates :active, inclusion: {in: [true, false]}
  validates :name, presence: true, length: {maximum: 1000}
   
  def details
    {       rate: self.rate,
     description: self.description,
              id: self.id,
          active: self.active,
            name: self.name    
      }
  end
  
  def initialize(params, hotel = nil)
    super(params)
    self.generate_id unless self.id
    self.hotel = hotel
    self.db = self.hotel.db if hotel
  end
  
  def calculate_amount(gross)
    response = {}
    amount = gross * self.rate
    response[:id] = self.id    
    response[:amount] = amount.format
    response[:amount_cents] = amount.cents
    return response
  end
  
  def self.calculate_total(rates)
    
    total_cents = 0
    rates.each do |rate|
      total_cents += rate[:amount_cents] 
    end
    response = {}
    response[:rates] = rates
    response[:total_cents] = total_cents
    response[:total] = Money.new(total_cents).format
    return response
  end
  
  def prefix
    return "trt"
  end
  
   def save
    return self.create_new unless self.hotel.find_tax_rate(self.id)
    if self.valid?
     return self.hotel.update_tax_rate(self.details)
    else
     return false
    end
   end
 
 def destroy
   self.hotel.destroy_tax_rate(self.id)
 end

 
 def create_new
   self.hotel.create_tax_rate(self.details)
 end
 
 def save!
   raise "Tax Rate failed to save" unless self.save
   return true
 end
  
  
end
