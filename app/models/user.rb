class User < CouchModel
  
  @@db = Couch.new(self.couch_base + "_users")

  
  attr_accessor :first_name, :last_name, :email, :type, :roles, :id, :hotel_id, :password, :password_scheme, :iterations, :derived_key, :salt, :username, :access_level
  validates :first_name, length: { in: 1..100 }
  validates :last_name, length: { in: 1..100 }  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }
  validates :type, presence: true
  validates :hotel_id, presence: true
  validates :password, length: {in: 4..20}
  validates :username, length: {in: 1..50}
  validate :unique_email
  validate :unique_username
    
  
  def initialize(params = {}, hotel = nil)
    self.username = params[:name] unless params[:username]
    params.delete(:name)
    super(params)
    self.roles = [hotel.id] if (params[:roles].nil? || params[:roles].empty?) && hotel   
    self.db = Couch.new(self.couch_base + "_users")
  end
  
  def self.find_by_token(token)
    cookies = {}
    cookies["AuthSession"] = token
    url = Rails.application.secrets.couchdb + "_sessions"
    begin
      result = RestClient.get 'http://127.0.0.1:5984/_session',{:content_type => 'application/x-www-form-urlencoded', :cookies => cookies}  
      result = JSON.parse(result, symbolize_names: true)
      if result[:userCtx][:name]
        return User.find_by_username(result[:userCtx][:name])
      else
        return nil  
      end
    rescue
      return nil
    end     
  end
  
  def self.find_by_email(email, include_doc = true)
   params = {include_docs: true, key: email}
   item = self.db.get_view("by_email", params)[0] 
   return self.new(item)[:doc] if item
   return nil
  end  
  
  def self.find_by_username(username, include_doc = true)
   params = {include_docs: true, key: username}
   item = self.db.get_view("by_username", params)[0]
   return self.new(item[:doc]) if item
   return nil
  end    
  
  def self.login(username, password)
    url = Rails.application.secrets.couchdb + "_sessions"
    begin
      result = RestClient.post 'http://127.0.0.1:5984/_session', "name=#{username}&password=#{password}",{:content_type => 'application/x-www-form-urlencoded'}  
    rescue
      return false
    end 
    response = JSON.parse(result, symbolize_names: true)
    response[:cookie] = result.cookies["AuthSession"]
    return response if response[:ok]
    return false
  end
  
  ## Getters/Setters ##
  
  
  
  def details
    {      _id: self.id,
    first_name: self.first_name,   
     last_name: self.last_name,
         email: self.email,
          name: self.username,          
      password: self.password,
          type: self.type,
         roles: [self.hotel_id],
      hotel_id: self.hotel_id,
  access_level: self.access_level    
       }
  end
  
  def login_details
    {
      first_name: self.first_name,
       last_name: self.last_name,
        hotel_id: self.hotel_id,
           email: self.email,
            type: self.access_level
    }
  end
  
  def id
     "org.couchdb.user:" + self.username.to_s
  end

  
  def name
    self.first_name + " " + self.last_name
  end
  
  
  def self.db
    @@db
  end
  
  ## Overwrite CouchModel Methods
  
  
  def prefix 
    "usr"
  end
  
  ## Validations
  
  def unique_email
    return true unless self.email
    errors.add(:email, I18n.t("api.duplicate", type: "User", field: "e-mail", value: self.email)) if User.find_by_email(self.email, true)
  end
  
  def unique_username
    return true unless self.username
    errors.add(:username, I18n.t("api.duplicate", type: "User", field: "username", value: self.username)) if User.find_by_username(self.username, true)
  end  
  
end  