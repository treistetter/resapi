class Rate < CouchModel
  
  attr_accessor :date, :currency, :rate_cents, :active, :roomtype_id, :tax_rate_ids, :rate 
  validates :currency, presence: true
  validates :date, presence: true
  validates :roomtype_id, presence: true
  validates :rate_cents, numericality: {only_integer: true, greater_than: 100, less_than: 1000000}
  validate :date_format
  
   
   def initialize(params = {}, hotel = nil)
     super(params)
     self.hotel = hotel
     self.db = hotel.db if hotel
     self.rate = Money.new(self.rate_cents)    
   end
   
    
   def self.resolve(roomtype_id, date, hotel)     
      date = date.to_s
      view_params = {limit: 1, startkey: [roomtype_id, date], endkey:  [roomtype_id, {}], include_docs: false}
      record = hotel.db.get_view("rates", view_params)
      return nil unless record[0]   
      rate = Rate.new(record[0][:value], hotel) 
      return rate
   end
   
   def self.find_by_id(id, hotel = nil)
      return nil unless hotel
      view_params = {limit: 1, startkey: id, endkey: id, include_docs: false}
      record = hotel.db.get_view("rates_by_id", view_params)
      return nil unless record[0]   
      rate = Rate.new(record[0][:value], hotel) 
      return rate     
   end
   
   def total
      response = {}
      response[:taxes] = []
      self.tax_rates.each do |tr|
         response[:taxes] << tr.calculate_amount(self.rate)
      end
      response[:taxes] = TaxRate.calculate_total(response[:taxes]) 
      response[:rate_cents] = self.rate.cents
      response[:rate] = self.rate.format
      response[:total_cents] = self.rate.cents + response[:taxes][:total_cents]
      response[:total] = Money.new(response[:total_cents]).format
      return response
   end
   
  def tax_rates
    return @tax_rates if !@tax_rates.nil? 
    return [] unless self.tax_rate_ids
    @tax_rates = []
    self.tax_rate_ids.each do |id|
      @tax_rates << self.hotel.find_tax_rate(id)
    end
    return @tax_rates
  end
      
   def details
     return {active: self.active,
               date: self.date,
           currency: self.currency,
         rate_cents: self.rate.cents,
               type: "rate", 
                 id: self.id,
       tax_rate_ids: self.tax_rate_ids,
        roomtype_id: self.roomtype_id      
       }
   end
   
  def prefix
    return "rat"
  end
  
  
  def date_format
    if self.date.is_a? Date
      return true if self.date > Date.today - 1
      errors.add(:rate, I18n.t("api.invalid_date", field: "Date")) 
      return false  
    else
      begin 
        test_date = Date.parse(self.date)
      rescue
        errors.add(:rate, I18n.t("api.invalid_date", field: "Date")) 
        return false        
      end
      return true if test_date > Date.today - 1
      errors.add(:rate, I18n.t("api.invalid_date", field: "Date")) 
      return false 
    end
  end
end
