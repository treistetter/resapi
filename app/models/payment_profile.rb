class PaymentProfile 

  def customer_profile
    @guest = self.guest
    @hotel = @guest.hotel 
    gateway = self.create_gateway_object
    gateway.get_customer_profile(customer_profile_id: self.profile_id)    
  end
  
  def get_cards
    result = self.customer_profile
    return {success: false, message: "Unknown error"} unless result.instance_variable_get(:@success)
    return {success: false, message: result.params["messages"]["message"]} unless result.params["profile"]["payment_profiles"]
    payment_profiles = result.params["profile"]["payment_profiles"]
    response = []
    if payment_profiles.is_a?(Array)
         payment_profiles.each do | pp |
            profile = {}
            profile[:payment_profile_id] = pp["customer_payment_profile_id"]
            profile[:card] = pp["payment"]["credit_card"]["card_number"].last(4)
            response << profile
         end    
         return response
    end
    
     if payment_profiles.is_a?(Hash)
            profile = {}
            profile[:payment_profile_id] = payment_profiles["customer_payment_profile_id"]
            profile[:card] = payment_profiles["payment"]["credit_card"]["card_number"].last(4)
            response << profile
            return response        
    end   
  end
  
  def get_payment_id(card)
    cards = self.get_cards
    card = card.to_s
    cards.each do |c|
      return c[:payment_profile_id] if c[:card] == card
    end
    return nil
  end
  
  def delete_profile
    @guest = self.guest
    @hotel = @guest.hotel     
    gateway = self.create_gateway_object
    response = gateway.delete_customer_profile(customer_profile_id: self.profile_id)
    
    if response.params["messages"]["result_code"]
       self.destroy
      return {success: true}
    else
      return {success: false, message: response..params["messages"]["message"]["text"]}
    end
    
    
  end  
  
  def add_card(params)
      return self.create_profile(params) if self.profile_id.nil?
      @guest = self.guest
      @hotel = @guest.hotel
      @gateway = self.create_gateway_object
      params[:card][:number] = params[:card][:number].to_i
      card = ActiveMerchant::Billing::CreditCard.new(params[:card])
      payment = {credit_card: card}
      address = params[:address]
      profile = {payment: payment, bill_to: address}
      
      if card.valid?
        response = @gateway.create_customer_payment_profile(customer_profile_id: self.profile_id, payment_profile: profile)
        if response.params["messages"]["result_code"] == "Ok"
           return {success: true, response: response}
        else
          return {success: false, message: response.message, cc_valid: true}
        end
    else
      return {success: false, messages: "The credit card is not valid", cc_valid: false }           
    end  
      
        
           
  end
  
  def create_profile(params)
      @guest = self.guest
      @hotel = @guest.hotel
      @gateway = self.create_gateway_object
      params[:card][:number] = params[:card][:number].to_i
      card = ActiveMerchant::Billing::CreditCard.new(params[:card])
      
      payment = {
      credit_card: card
    }
    
    profile = {
      merchant_customer_id: @guest.uuid,
      description: @guest.uuid,
      email: @guest.email,
      payment_profiles: {
        payment: payment, 
        bill_to: @guest.billing_address, 
        customer_type: 'individual'
        }
     }

      
      if card.valid?
        response = @gateway.create_customer_profile(profile: profile)
        if response.params["messages"]["result_code"] == "Ok"
           self.profile_id = response.params["customer_profile_id"]
           self.save
           return {success: true, response: response}
        else
          return {success: false, message: response.message, cc_valid: true}
        end
    else
      return {success: false, messages: "The credit card is not valid", cc_valid: false }           
    end  
  end
    
  def create_gateway_object
    
           @hotel.cc_sandbox ? login = ENV['AUTHORIZENETLOGIN'] : login = @hotel.gateway_login
           @hotel.cc_sandbox ? password = ENV['AUTHORIZENETPASSWORD'] : password = @hotel.gateway_password
    
    ActiveMerchant::Billing::AuthorizeNetCimGateway.new(
            login: login,
         password: password,
             test: @hotel.cc_sandbox
    )
  
  end
  
  def get_customer_profile
    @guest = self.guest
    @hotel = @guest.hotel
    gateway = self.create_gateway_object
    return gateway.get_customer_profile(customer_profile_id: self.profile_id)   
  end
end
