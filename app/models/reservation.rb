class Reservation < CouchModel

  include Invoice  
  attr_accessor :delete_occupancies, :invalid_delete_occupancies, :modify_occupancy_errors, :cancelled_at, :cancelled_by_id, :cancelled, :cancel_reason, :payments, :finalized, :guest, :occupancies, :create_hold, :extra_ids, :start, :end, :cancel_policy
  
  validates :cancelled_at, presence: true, if: :cancelled
  validates :cancelled_by_id, presence: true, if: :cancelled
  validate :valid_guest, if: :finalized
  validate :valid_occupancies
  validate :valid_extras
  validate :no_duplicate_occupancies, if: :valid_occupancies
  
  def initialize(params, hotel)
    params.delete(:invoice)
    params.delete(:stays)
    super(params,hotel)
    self.extra_ids = [] unless params[:extra_ids]
    self.payments = []
    
    if params[:guest] && !params[:guest].is_a?(Guest)
      self.guest = Guest.new(params[:guest])
    else
      self.guest = params[:guest]  
    end
    
    if params[:payments]
      params[:payments].each do |payment|
        if payment.is_a?(Payment)
           self.payments << payment
        else
           self.payments << Payment.new(payment, self.hotel)
        end
      end
    end
    
    self.occupancies = []
    if params[:occupancies]
      params[:occupancies].each do |occupancy|
        if occupancy.is_a?(Occupancy)
          self.occupancies << occupancy
        else
          self.occupancies << Occupancy.new(occupancy, self.hotel)          
        end
      end
    end   
  end
  

  def details
    details = {}
    details[:invoice] = self.invoice_with_payments    
    details[:stays] = self.stays
    details[:guest] = self.guest.details if self.guest
    details[:occupancies] = self.occupancy_details
    details[:start] = self.start_date.to_s
    details[:end] = self.end_date.to_s
    details[:id] = self.id
    details[:cancel_policy] = self.cancel_policy
    details[:type] = "reservation"
    details[:payments] = self.payment_details
    details[:extra_ids] = self.extra_ids
    details[:finalized] = self.finalized

    if self.cancelled
      details[:cancelled] = self.cancelled
      details[:cancelled_at] = self.cancelled_at
      details[:cancel_reason] = self.cancel_reason
      details[:cancelled_by_id] = self.cancelled_by_id      
    end
    return details
  end
  

  
  ## Process ## 
  
  def delete_occupancies(ids = [])
     return true if ids.nil? || ids.empty?
     current_length = self.occupancies.length
     error_flag = false
     self.invalid_delete_occupancies = []
     ids.each do |id|
       self.occupancies.delete_if {|occ| occ.id == id}
       if self.occupancies.length == current_length
          error_flag = true
          self.invalid_delete_occupancies << id
          next
       end
       current_length = self.occupancies.length
     end
     return !error_flag
   end
   
   def create_occupancies(occs = [])

     return true if occs.empty? 
     occs.each do |occ| 
       self.occupancies << Occupancy.new({guests: occ[:guests], room_id: occ[:room_id], date: occ[:date], extra_ids: occ[:extra_ids]}, self.hotel)
     end 
   end
   
   def delete_extras(ids = [])
      return true if ids.empty?
      current_extra_count = self.extra_ids.length
      delete_extra_count = ids.length
      self.extra_ids = self.extra_ids - ids
      return true if self.extra_ids.length == current_extra_count - delete_extra_count
      self.invalid_delete_extras = self.received_delete_extras - self.reservation.extra_ids 
      return false
   end
   
   def modify_occupancies(occs = [])
     return true if occs.empty?
     
     fail_flag = false
     errors = []
     
     occs.each do |request|
       occupancy = nil
       
       if request[:id].nil?
         fail_flag = true
         errors << "Modify occupancy request received with no occupancy ID"
         next
       end
       
       self.occupancies.each do |occ|
          if occ.id == request[:id]
            occupancy = occ
            break
          end
       end
       
       if occupancy.nil?
         fail_flag = true
         errors << "Occupancy with ID #{request[:id]} not found"
         next
       end
             
       if request[:guests]
          occupancy.guests = request[:guests]
          rate = occupancy.resolve_rate
          if rate.nil?
            fail_flag = true
            errors << "Could not resolve rate for occupancy #{occupancy.id}.  Date was #{occupancy.date.to_s}, guests: #{occupancy.guests}, and room ID: #{occupancy.room_id}"
          end
          occupancy.rate = rate
       end
       
       if !request[:extras].nil?
          occupancy.extra_ids = request[:extras]
       end
     end
     return true unless fail_flag
     self.modify_occupancy_errors = errors
     return false
   end
  
  def cancel(user, reason = nil)
    Reservation.transaction do
      self.occupancies.each do |occ|
        occ.update_attributes!(cancelled: true)
      end      
      self.cancel_confirmation = SecureRandom.hex(4)
      self.cancelled_at = Time.now
      self.cancelled_by_id = user.id
      self.cancel_reason = reason
      self.cancelled = true
      self.save!
    end
    return self.cancel_confirmation
  end
  
  
  ## Getters 

  def due_on_cancel
    @guest = self.guest
    @hotel = @guest.hotel    
    invoice = self.invoice_with_payments
    percentage_due = self.cancel_percentage.to_f
    total_paid = (invoice[:total_paid].to_f * 100).round(0)
    total_cost = (invoice[:cost_total].to_f * 100).round(0)
    total_due = (total_cost * percentage_due).round(0)
    outstanding_due =  total_due - total_paid
    return outstanding_due    
  end
  
  def cancel_percentage(days = self.days_until_start)   
   return 0.0 unless self.cancel_policy
   cancel_array = self.cancel_policy.sort_by {|h| h[:days]}
   cancel_array.each do | ca | 
      return ca[:percent] if days <= ca[:days]
   end
   return 0.0
  
  end

  def start_date
     occupancies = self.occupancies
     occupancies = occupancies.sort_by { |o| o.date}
     return occupancies[0].date
  end
  
  def days_until_start
    (self.start_date - Date.today).to_i
  end
  
  def end_date
     occupancies = self.occupancies
     occupancies = occupancies.sort_by { |o| o.date}
     Rails.logger.info "occupancy date is"
     Rails.logger.info occupancies.last.inspect
     return occupancies.last.date + 1    
  end
  
  def cancelled_by
    User.find_by_id(self.cancelled_by_id)
  end
  
  def is_cancelled?
    self.cancelled
  end
  
  def is_finalized?
    self.finalized
  end
  

      
  def occupancy_details
    response = []
    self.occupancies.each do |occupancy|
      response << occupancy.details
    end
    return response
  end
  
  def payment_details
    response = []
    self.payments.each do |payment|
      response << payment.details
    end
    return response
  end
  
  
  def extras
    return @extras unless @extras.nil?
    @extras = []
    self.extra_ids.each do |id|
      @extras << self.hotel.find_extra(id)
    end
    return @extras
  end

 
  def prefix
    return "res"
  end  
  
  ## Validations ##
    
  def valid_guest
    unless self.guest
      errors.add(:guest, "can not be blank")
      return false
    end
    unless self.guest.valid?
      errors.add(:guest, self.guest.errors.messages)
    end
  end
  
  def no_duplicate_occupancies
    sorted_occupancies = self.occupancies.sort_by {|o| [o.room_id, o.date]}
    found_duplicates = false
    messages = []
    
    sorted_occupancies.each_with_index do |occupancy, i|      
      next if i == sorted_occupancies.length - 1
      if occupancy.room_id == sorted_occupancies[i+1].room_id && occupancy.date == sorted_occupancies[i+1].date
         found_duplicates = true
         messages << {date: "Room #{occupancy.room_id} booked twice on #{occupancy.date.to_s}"}
      else
         messages << {}
      end
    end
    
    if found_duplicates
      messages.each do |message|
        self.errors[:occupancies] << message
      end
    end
  end
  
  def valid_occupancies
    return @valid_occupancies unless @valid_occupancies.nil?
    
    if self.cancelled
      @valid_occupancies = true
      return true
    end
    
    if self.occupancies.empty?                                                                                     
      errors.add(:occupancies, "Occupancies can not be empty") 
      @valid_occupancies = false
      return false
    end
    
    messages = []
    found_errors = false
    self.occupancies.each do |occupancy|
      occupancy = Occupancy.new(occupancy, hotel) unless occupancy.is_a? Occupancy
      if occupancy.valid?
        messages << {}
      else  
        found_errors = true
        messages << occupancy.errors.to_hash(full_messages: true) unless occupancy.valid?
      end      
    end
    
    if found_errors
      messages.each do |message|
        self.errors[:occupancies] << message
      end
    end
    
    @valid_occupancies = !found_errors
    return @valid_occupancies
  end
  
  def valid_extras
    return true if self.cancelled
    messages = []
    found_error = false
    self.extra_ids.each do |id|
      extra = self.hotel.find_extra(id)
      if extra
        if extra.reservation
          messages << ""
        else
          messages << "Extra #{id} is not a valid reservation extra"
          found_error = true 
        end
      else
        messages << "Extra #{id} does not exist"
        found_error = true          
      end
    end

    if found_error
      messages.each do |message|
        self.errors[:extras] << message
      end
    end
  end
  
  private
  
  
  def generate_cancel_token
      self.cancel_token = SecureRandom.hex(8)
  end  
       
end
