class Guest 
  
  include ActiveModel::Model
  
  attr_accessor :first_name, :last_name, :email, :phone, :phone_secondary, :address_details
  
  validates :first_name, presence: true
  validates :last_name, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }
  validates :email, format: { with: VALID_EMAIL_REGEX }, if: :email
  validate :valid_address?
  

  def details
    
    phone = nil
    phone_secondary = nil
    phone = self.phone.gsub(/[^0-9]/, "") if self.phone
    phone_secondary = self.phone_secondary.gsub(/[^0-9]/, "") if self.phone_secondary    
    response = {first_name: self.first_name,
             last_name: self.last_name,
                 email: self.email,
                 phone: phone,
       phone_secondary: phone_secondary   
      }
      response[:address_details] = self.address.nil? ? nil : self.address.details
      return response
  end
  
  def address= (address)
    @address = nil unless address
    if address.is_a?(Address)
      @address = address
      return @address
    end
    @address = Address.new(address)
  end
  
  def address
    return @address if @address
    @address = Address.new(self.address_details)
    return @address
  end
  
  def valid_address?
    return true unless self.address
    unless self.address.valid?
      errors.add(:address, self.address.errors.messages.to_s)
    end
  end
  
  def prefix
    return "gst"
  end
  
end
