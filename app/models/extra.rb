class Extra < CouchModel

  attr_accessor :description, :label, :price_cents, :taxes, :active, :reservation, :room, :tax_rate_ids, :pricing   
    
    validates :description, presence: true
    validates :label, presence: true
    validates :price_cents, presence: true, numericality: {only_integer: true}
    validates :active, inclusion: { in: [true, false] }
    validates :reservation, inclusion: { in: [true, false] }
    validate  :tax_rate_ids_array?
    
    

    
  def initialize(params, hotel = nil)
    super(params)
    self.generate_id unless self.id
    self.hotel = hotel
    self.db = self.hotel.db if hotel
  end  
  
  def details
    return {id: self.id, 
         label: self.label, 
       pricing: self.calculate_cost,
   description: self.description,
        active: self.active,
          room: self.room,
   price_cents: self.price_cents,
  tax_rate_ids: self.tax_rate_ids,
   reservation: self.reservation    
       }
  end  
  
  def tax_rates
    return @tax_rates if @tax_rates
    return [] unless self.tax_rate_ids
    @tax_rates = []
    self.tax_rate_ids.each do |id|
      @tax_rates << self.hotel.find_tax_rate(id)
    end
    return @tax_rates
  end
  
  def price
    Money.new(self.price_cents)
  end
  
  def calculate_cost
        taxes = []
        self.tax_rates.each do |tax|
          taxes << tax.calculate_amount(self.price)
        end
        response = {}
        response[:id] = self.id
        response[:taxes] = TaxRate.calculate_total(taxes)
        response[:gross_cents] = self.price_cents
        response[:gross] = self.price.format
        response[:total_cents] = response[:taxes][:total_cents] + self.price_cents
        response[:total] = Money.new(response[:total_cents]).format
        return response
    end
    
  def self.total_extras(extras)
    if extras.nil? || extras.empty?
       zero = Money.new(0).format
       return {total: zero, total_cents: 0, total_taxes: zero, total_taxes_cents:0, extras: [], gross_cents: 0, gross: zero}  
    end
    total_cents = 0
    total_taxes_cents = 0
    gross = 0
    
    response = {}
    response[:extras] = []
    
    extras.each do |extra|
      extra_cost = extra.calculate_cost
      response[:extras] << extra_cost
      total_cents += extra_cost[:total_cents]
      total_taxes_cents += extra_cost[:taxes][:total_cents]
      gross += extra_cost[:gross_cents] 
    end
    
    response[:gross_cents] = gross
    response[:gross] = Money.new(gross).format    
    response[:total_taxes_cents] = total_taxes_cents
    response[:total_taxes] = Money.new(total_taxes_cents).format
    response[:total_cents] = total_cents
    response[:total] = Money.new(total_cents).format
    return response
  end

 def save
   return self.create_new unless self.hotel.find_extra(self.id)
   if self.valid?
     return self.hotel.update_extra(self.details)
   else
     return false
   end
 end
 
 def destroy
   self.hotel.destroy_extra(self.id)
 end

 
 def create_new
   self.hotel.create_extra(self.details)
 end
 
 def save!
   raise "Extra failed to save" unless self.save
   return true
 end
  
  def prefix
    return "ext"
  end



  
  def tax_rate_ids_array?
    return true if self.tax_rate_ids.is_a?(Array)
    errors.add(:tax_rate_ids, "Tax Rate IDs must be an array")
  end
  
 
end
