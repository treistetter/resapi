class Payment < CouchModel
    
  attr_accessor :amount_cents, :card, :method, :success, :reservation_id, :description, :response, :captured, :transaction_id, :authorization, :response_code, :response_reason_code, :options, :credit_card, :gateway
  
  def details
    return {
         amount_cents: self.amount_cents,
        authorization: self.authorization,
                 card: self.card, 
             captured: self.captured,    
          description: self.description, 
                   id: self.id, 
               method: self.method, 
       reservation_id: self.reservation_id,
             response: self.response,
        response_code: self.response_code,    
 response_reason_code: self.response_reason_code,    
              success: self.success, 
       transaction_id: self.transaction_id,
              type: "payment"
        }
  end
  
  def amount
    return @amount if @amount
    @amount = Money.new(self.amount_cents)
    return @amount
  end
  
  def prefix 
    "pmt"
  end
  
  def prepare_for_method(params)
    self.options = params[:options].nil? ?  {} : params[:options]
    self.options[:order_id] = self.id
    self.create_gateway_object
    if params[:card].is_a?(ActiveMerchant::Billing::CreditCard)
      self.credit_card = params[:card]
    else
      params[:card][:number] = params[:card][:number].to_i
      self.credit_card = ActiveMerchant::Billing::CreditCard.new(params[:card])      
    end
    self.options[:billing_address] = params[:billing_address].billing_format if params[:billing_address]
    self.description = params[:description]
    self.card = self.credit_card.number.to_s.last(4)
  end
  
  def authorize_and_capture(params)
    return {success: false, message: "This payment has already been processed"} unless self.success.nil?
    self.prepare_for_method(params)
    if self.credit_card.valid?
        response = self.gateway.purchase(self.amount_cents, self.credit_card, self.options)
        if response.success?
           self.update_attributes(response: response, 
                                      success: true, 
                                       method: "authorize_and_capture", 
                               transaction_id: response.params["transaction_id"],       
                                response_code: response.params["response_code"], 
                         response_reason_code: response.params["response_reason_code"])
           return {success: true, response: response}
        else
           self.update_attributes(response: response, 
                                      success: false, 
                                       method: "authorize_and_capture", 
                                response_code: response.params["response_code"], 
                               transaction_id: response.params["transaction_id"],
                         response_reason_code: response.params["response_reason_code"])
           return {success: false, message: response.message, response_code: response.params["response_code"], response_reason_code: response.params["response_reason_code"], response_reason_text: response.params["response_reason_text"], cc_valid: true}
        end
    else
      self.update_attributes(response: "Invalid credit card submitted.  Method not attempted.", success: false, method: "authorize_and_capture")
      return {success: false, messages: "The credit card is not valid", cc_valid: false }           
    end    
  end
  
  def authorize(params)
    return {success: false, message: "This payment has already been processed"} unless self.success.nil?
    self.prepare_for_method(params)

    if self.credit_card.valid?
        response = self.gateway.authorize(self.amount_cents, self.credit_card, self.options)
        if response.params["response_code"] == 1 # handle success
           self.update_attributes(response: response, 
                             response_code: response.params["response_code"], 
                                   success: true, 
                                    method: "authorize",
                                  captured: false,  
                            transaction_id: response.params["transaction_id"],
                             authorization: response.authorization ) 
           return {success: true, response: response}
        else
          self.update_attributes(response: response, 
                                  success: false, 
                                 captured: false,
                                   method: "authorize", 
                            response_code: response.params["response_code"],
                     response_reason_code: response.params["response_reason_code"]                           
                                ) 
          return {success: false, 
                  message: response.message, 
            response_code: self.response_code, 
     response_reason_code: self.response_reason_code, 
     response_reason_text: response.params["response_reason_text"],  
                 cc_valid: true}
        end
    else
      self.update_attributes(response: ActiveMerchant::Billing::Response.new(false, "Invalid card information submitted.  Please correct information and resubmit"), success: false, method: "authorize" ) 
      return {success: false, message: self.response.message, cc_valid: false }           
    end  
    
  end
  
  def capture(payment)
    return {success: false, message: "No authorized payment passed in to capture method"} unless payment    
    return {success: false, message: "Attempted to process a payment that has already been processed"} unless self.success.nil?
    return {success: false, message: "Attempted to capture an authorization that has already been captured"} if payment.captured
    return {success: false, message: "Attempted to capture an amount greater than authorized"} if self.amount_cents > payment.amount_cents
    return {success: false, message: "Attempted to capture an authorization that failed"} unless payment.success
    return {success: false, message: "Attempted to capture a payment that was not an authorize transaction"} unless payment.method == "authorize"
    return {success: false, message: "Attempted to capture an authorization without an authorization id"} unless payment.authorization
   

    self.create_gateway_object
    response = self.gateway.capture(self.amount_cents, payment.authorization)
    if response.params["response_code"] == 1
      self.update_attributes(card: payment.card,
                    response_code: response.params["response_code"],
             response_reason_code: response.params["response_reason_code"],
                   transaction_id: response.params["transaction_id"],
                          success: true      
                            )
      
      return { success: true, response: response }
      
    else
      self.update_attributes(card: payment.card,
                    response_code: response.params["response_code"],
             response_reason_code: response.params["response_reason_code"],
                   transaction_id: response.params["transaction_id"],
                          success: true      
                            )
      return    {       success: false,
                        message: response.params["response_reason_code"],
                  response_code: response.params["response_code"],
           response_reason_code: response.params["response_reason_code"],
   
              }    
    end    
  end


  def create_gateway_object
    self.hotel.cc_sandbox ? login = Rails.application.secrets.AUTHORIZENETLOGIN : login = self.hotel.gateway_login
    self.hotel.cc_sandbox ? password = Rails.application.secrets.AUTHORIZENETPASSWORD : password = self.hotel.gateway_password
    self.gateway = ActiveMerchant::Billing::AuthorizeNetGateway.new(
            login: login,
         password: password,
             test: self.hotel.cc_sandbox
    )
  end

  
  def amount_charged_cents
     return 0 unless self.success
     return 0 if self.method == "authorize"
     return self.amount_cents
  end
  
  def amount_charged
     return Money.new(self.amount_charged_cents)
  end
  
end