module Invoice
    
  def stays
      rooms = []
      roomarray = []
      roomhash = {}
     
      self.room_objects.each do |rm|
        roomhash[rm.id.to_sym] = {}
        roomhash[rm.id.to_sym][:occupancies] = [] # create occupancy array for each room       
      end
         
      self.occupancies.each do |occ|
       roomhash[occ.room_id.to_sym][:occupancies]  << occ  # push each occupancy into the appropriate array  
      end
      
      response = []
      
      self.room_objects.each do | rm |
         current_stay = {}
         current_stay[:room] = rm.id
        
         if roomhash[rm.id.to_sym][:occupancies].length == 1 # Room is booked for just one night, so create single stay and move on to next room           
            current_stay[:check_in] = roomhash[rm.id.to_sym][:occupancies][0].date.to_s
            current_stay[:check_out] = (roomhash[rm.id.to_sym][:occupancies][0].date + 1).to_s
            current_stay[:guests] = roomhash[rm.id.to_sym][:occupancies][0].guests
            response << current_stay
            next
         end
                          
         roomhash[rm.id.to_sym][:occupancies].sort! {|a, b| a.date <=> b.date} # sort occupancies by date
         current_occ = roomhash[rm.id.to_sym][:occupancies][0]
         occ_length = roomhash[rm.id.to_sym][:occupancies].length
         current_stay[:check_in] = current_occ.date.to_s       
         current_stay[:guests] = current_occ.guests
         roomhash[rm.id.to_sym][:occupancies].each_with_index do |occ, i|
           next if i == 0 # skip first iteration since current_occ is first occ.  
           if occ.date == current_occ.date + 1 # We are still in consecutive nights                   
             if i == occ_length - 1 # we are at the last occupancy and so it is the last in the sequence.  Set check_out of current_stay. Last item so iteration is over
                current_stay[:check_out] = (occ.date + 1).to_s
                response << current_stay
             else # We are still in the sequence and have more occupancie. Set new current_occ and go to next iteration 
               current_occ = occ
               next
             end            
          else # This occupancy is not consecutive with the last night
            if i == occ_length - 1 # We are looking at the last night.  Since it is not part of the current sequence, we need to end previous sequence and add a single night stay
               current_stay[:check_out] = (roomhash[rm.id.to_sym][:occupancies][i-1].date + 1).to_s # Get previous occupancy to end previous sequence
               response << current_stay  
               response << {room: rm.id, check_in: occ.date.to_s, check_out: (occ.date + 1).to_s, guests: occ.guests} # Create the single night stay            
            else # The current sequence has ended, and we have more to look at
               current_stay[:check_out] = (roomhash[rm.id.to_sym][:occupancies][i-1].date + 1).to_s # set previous sequence checkout
               response << current_stay
               current_occ = occ 
               current_stay = {room: rm.id, check_in: current_occ.date.to_s, guests: current_occ.guests} # start next sequence
            end    
          end 
        end 
      end       
      return response
  end
  
  def room_objects
      unique_room_occs = self.occupancies.to_a.uniq {|o| o.room_id} 
      rooms = []
      unique_room_occs.each do |occ|
        rooms << self.hotel.find_room(occ.room_id)
      end
      return rooms
  end
  
  def invoice_with_payments
     invoice = self.invoice

     payments = []
     total_payment = 0
     
     self.payments.each do | payment|       
       payments << payment.details
       total_payment += payment.amount_charged_cents
     end

     balance_outstanding = invoice[:total_cents] - total_payment
     invoice[:payments] = payments  
     invoice[:balance_outstanding_cents] = balance_outstanding
     invoice[:balance_outstanding] = Money.new(balance_outstanding).format
     invoice[:paid_cents] = total_payment
     invoice[:paid] = Money.new(total_payment).format
     
     return invoice   
  end
  

  
  def invoice_with_deposit
     invoice = self.invoice
     deposit = self.hotel.deposit_rate ?  (Money.new(invoice[:total_cents]) * self.hotel.deposit_rate) :  Money.new(0)
     invoice[:deposit_due] = deposit.format
     invoice[:deposit_due_cents] = deposit.cents
     return invoice
  end
  

  def occupancy_line_items
        occupancylines = []
        self.occupancies.each do | occ |
           occupancylines << occ.details
        end
        return occupancylines   
  end
  


  def invoice     
        response = {}
        response[:occupancy] = Occupancy.total_occupancies(self.occupancies)
        response[:reservation_extras] = Extra.total_extras(self.extras)
        response[:total_cents] = response[:occupancy][:total_cents] + response[:reservation_extras][:total_cents]
        response[:total] = Money.new(response[:total_cents]).format
        response[:total_taxes_cents] = response[:occupancy][:total_taxes_cents] + response[:reservation_extras][:total_taxes_cents]
        response[:total_taxes] = Money.new(response[:total_taxes_cents]).format   
        response[:gross_cents] = response[:occupancy][:gross_cents] + response[:reservation_extras][:gross_cents]     
        response[:gross] = Money.new(response[:gross_cents]).format
        response[:total_extras_cents] = response[:reservation_extras][:gross_cents] + response[:occupancy][:gross_extras_cents]
        response[:total_extras] = Money.new(response[:total_extras_cents]).format
        return response
  end      
  
  def start_date
     occupancies = self.occupancies
     occupancies.sort_by!{|o| o.date}
     return occupancies[0].date
  end
  

end