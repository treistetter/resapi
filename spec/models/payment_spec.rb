require 'rails_helper'

describe Payment do
  
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    @guest = FactoryGirl.build(:guest)
    @card = Card.generate
    @card_last_4 = @card[:number].to_s.last(4)
    @payment = Payment.new({amount_cents: 1000, card: @card, }, @hotel)
    #@payment_profile = PaymentProfile.new
    #@payment_profile.add_card(card: @card, address: @guest.billing_address)
  end


  it "Should be valid given valid information" do
     expect(@payment).to be_valid
  end
  
  describe '.amount_charged_cents' do
    
    it 'should return 0 for authorize methods' do
      @payment.method = "authorize"
      @payment.success = true
      expect(@payment.amount_charged_cents).to eq(0)
    end
    
    it 'should return the charged amount for captured methods' do
      @payment.method = "capture"
      @payment.success = true
      expect(@payment.amount_charged_cents).to eq(1000)      
    end
    
    it 'should return the charged amount for authorize_and_capture methods' do
      @payment.method = "authorize_and_capture"
      @payment.success = true
      expect(@payment.amount_charged_cents).to eq(1000)      
    end
    
    it 'should return 0 if the payment failed' do
      @payment.method = "capture"
      @payment.success = false
      expect(@payment.amount_charged_cents).to eq(0)      
    end
    
  end
  
  
  ## AUTHORIZE ##
  
  describe '.authoirze(params)' do
     it "should return success = true given valid information" do
        result = @payment.authorize(card: @card)
        expect(result[:success]).to eq(true)   
     end
  
     it "should store the last four digits of the credit card" do
        @payment.authorize(card: @card)
        expect(@payment.card).to eq("1111")
     end
  
     it "should not authorize a payment that has already been processed" do
         @payment.authorize(card: @card)
         result = @payment.authorize(card: @card)
         expect(result[:success]).to eq(false)
     end
  
     it "should return an error message if attempting to authorize a payment that has already been processed" do
         @payment.authorize(card: @card)
         result = @payment.authorize(card: @card)
         expect(result[:message]).to eq("This payment has already been processed")
     end
  
     it "should store the authorization code given valid information" do
       @payment.authorize(card: @card)      
       expect(@payment.authorization).to_not be_nil
     end
  
     it "should return success = false if the payment fails" do
       address = Card.general_error_address
       @card = Card.general_error
       @payment = Payment.new({amount_cents: 1200}, @hotel)
       result = @payment.authorize(card: @card, billing_address: address)
       expect(result[:success]).to eq(false)    
     end
    
     it "should return a response code if the payment fails" do
       address = Card.general_error_address
       @card = Card.general_error
       result = @payment.authorize(card: @card, billing_address: address)
        expect(result[:response_code]).to eq(2)   
     end
  
     it "should return a response reason code if the payment fails" do
       address = Card.general_error_address
       @card = Card.general_error
       result = @payment.authorize(card: @card, billing_address: address)
       expect(result[:response_reason_code]).to eq('2')
     end
  
     it "should return a response reason message if the payment fails" do
       address = Card.general_error_address
       @card = Card.general_error
       result = @payment.authorize(card: @card, billing_address: address)
       expect(result[:response_reason_text]).to_not be_nil    
     end
  
     it "should return success = false if the credit card is invalid" do
       @card[:number] = "Invalid"
       result = @payment.authorize(card: @card)
       expect(result[:success]).to eq(false)    
     end
  
     it "should return cc_valid = false if the credit card is invalid" do
       @card[:number] = "Invalid"
       result = @payment.authorize(card: @card)
       expect(result[:cc_valid]).to eq(false)      
     end
  
     it "should not authorize a payment that has already been processed" do
         @payment.authorize(card: @card)
         result = @payment.authorize(card: @card)
         expect(result[:success]).to eq(false)
     end
  
     it "should return an error message if attempting to authorize a payment that has already been processed" do
         @payment.authorize(card: @card)
         result = @payment.authorize(card: @card)
         expect(result[:message]).to eq("This payment has already been processed")
     end
  
     it "should set the method to authorize" do
       @payment.authorize(card: @card)
       expect(@payment.method).to eq("authorize") 
     end
 end
  
   
  ## Capture 
  describe '.capture' do
    
     before(:each) do
       @payment.authorize(card: @card)
       @payment = @hotel.find_payment(@payment.id)       
     end
      
     it "should successfully capture a prior authorization" do
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:success]).to eq(true)
     end
  
     it "should store the transaction_id if the capture succeeds" do
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       capture_payment.capture(@payment)
       capture_payment = @hotel.find_payment(capture_payment.id)
       expect(capture_payment.transaction_id).to_not be_nil    
     end
    
     it "should not attempt to capture an amount greater than the authorization" do
       capture_payment = Payment.new({amount_cents: 1200}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:success]).to eq(false)
     end
  
     it "should return an appropriate error message if the amount is greater than the authorization" do
       capture_payment = Payment.new({amount_cents: 1200}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:message]).to eq("Attempted to capture an amount greater than authorized")    
     end
  
     it "should not attempt to capture if the payment is older than 30 days" do
    
     end
  
     it "should not attempt to capture unless the authorization succeeded" do
       @payment.update_attributes(success: false)
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:success]).to eq(false)    
     end
  
     it "should not attempt to capture an authorization that has already been captured" do
       @payment.update_attributes(captured: true)       
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:success]).to eq(false)        
     end
  
     it "should return an appropriate error message if attempting to capture an authorization that has already been captured" do
       @payment.update_attributes(captured: true)       
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:message]).to eq("Attempted to capture an authorization that has already been captured")       
     end
  
     it "should not attempt to process a payment that has already been processed" do
       @payment.update_attributes(captured: true)       
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       capture_payment.success = true
       capture_payment.save
       result = capture_payment.capture(@payment)
       expect(result[:success]).to eq(false)      
     end
  
     it "should return an appropriate error message if attempting to process a payment that has already been processed" do
       @payment.update_attributes(captured: true)       
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       capture_payment.success = true
       capture_payment.save
       result = capture_payment.capture(@payment)
       expect(result[:message]).to eq("Attempted to process a payment that has already been processed")           
     end
  
     it "should return an appropriate error message if the authorization failed" do
       @payment.update_attributes(success: false)
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:message]).to eq("Attempted to capture an authorization that failed")       
    end
  
     it "should not attempt to capture unless the payment has an authorization value" do
       @payment.update_attributes(authorization: nil)
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:success]).to eq(false)    
     end
  
     it "should return an appropriate error message if the payment has no authorization value" do
       @payment.update_attributes(authorization: nil)
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:message]).to eq("Attempted to capture an authorization without an authorization id") 
     end
  
     it "should not attempt to capture unless the payment method is authorize" do
       @payment.method = "authoirze_and_capture"
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:success]).to eq(false)        
     end
  
     it "should return an appropriate error message if the payment method is not authorize" do
       @payment.method = "authoirze_and_capture"
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:message]).to eq("Attempted to capture a payment that was not an authorize transaction")       
     end
  
     it "should return success: false if the capture fails" do
       @payment.authorize(card: @card)
       @payment.update_attributes(authorization: "12345")
       capture_payment = Payment.new({amount_cents: 800}, @hotel)
       result = capture_payment.capture(@payment)
       expect(result[:success]).to eq(false) 
     end
  end
  
  ## Authorize and Capture ##
  
    describe '.authorize_and_capture(params)' do
     it "should return success = true given valid information" do
        result = @payment.authorize_and_capture(card: @card)
        expect(result[:success]).to eq(true)   
     end
  
     it "should store the last four digits of the credit card" do
        @payment.authorize_and_capture(card: @card)
        expect(@payment.card).to eq("1111")
     end
  
     it "should not authorize a payment that has already been processed" do
         @payment.authorize_and_capture(card: @card)
         result = @payment.authorize_and_capture(card: @card)
         expect(result[:success]).to eq(false)
     end
  
     it "should return an error message if attempting to authorize a payment that has already been processed" do
         @payment.authorize_and_capture(card: @card)
         result = @payment.authorize_and_capture(card: @card)
         expect(result[:message]).to eq("This payment has already been processed")
     end
  
     it "should store the transaction id given valid information" do
       @payment.authorize_and_capture(card: @card)      
       expect(@payment.transaction_id).to_not be_nil
     end
  
     it "should return success = false if the payment fails" do
       address = Card.general_error_address
       @card = Card.general_error
       @payment = Payment.new({amount_cents: 1200}, @hotel)
       result = @payment.authorize_and_capture(card: @card, billing_address: address)
       expect(result[:success]).to eq(false)    
     end
    
     it "should return a response code if the payment fails" do
       address = Card.general_error_address
       @card = Card.general_error
       result = @payment.authorize_and_capture(card: @card, billing_address: address)
        expect(result[:response_code]).to eq(2)   
     end
  
     it "should return a response reason code if the payment fails" do
       address = Card.general_error_address
       @card = Card.general_error
       result = @payment.authorize_and_capture(card: @card, billing_address: address)
       expect(result[:response_reason_code]).to eq('2')
     end
  
     it "should return a response reason message if the payment fails" do
       address = Card.general_error_address
       @card = Card.general_error
       result = @payment.authorize_and_capture(card: @card, billing_address: address)
       expect(result[:response_reason_text]).to_not be_nil    
     end
  
     it "should return success = false if the credit card is invalid" do
       @card[:number] = "Invalid"
       result = @payment.authorize_and_capture(card: @card)
       expect(result[:success]).to eq(false)    
     end
  
     it "should return cc_valid = false if the credit card is invalid" do
       @card[:number] = "Invalid"
       result = @payment.authorize_and_capture(card: @card)
       expect(result[:cc_valid]).to eq(false)      
     end
  
     it "should not authorize a payment that has already been processed" do
         @payment.authorize_and_capture(card: @card)
         result = @payment.authorize_and_capture(card: @card)
         expect(result[:success]).to eq(false)
     end
  
     it "should return an error message if attempting to authorize a payment that has already been processed" do
         @payment.authorize_and_capture(card: @card)
         result = @payment.authorize_and_capture(card: @card)
         expect(result[:message]).to eq("This payment has already been processed")
     end
  
     it "should set the method to authorize_and_capture" do
       @payment.authorize_and_capture(card: @card)
       expect(@payment.method).to eq("authorize_and_capture") 
     end
 end
end
