require 'rails_helper'

describe Rate do
  

    
    describe 'Rate.resolve(roomtype, date, hotel)' do
      before(:each) do
        @hotel = FactoryGirl.create(:hotel)
        @rt = FactoryGirl.build(:roomtype, hotel: @hotel)
        @rt = @hotel.create_roomtype(@rt.details)
        @rate1 = FactoryGirl.build(:rate, date: Date.today.to_s, roomtype_id: @rt.id, hotel: @hotel)
        @rate2 = FactoryGirl.build(:rate, date: (Date.today + 3).to_s, roomtype_id: @rt.id,  hotel: @hotel)
        @rate3 = FactoryGirl.build(:rate, date: (Date.today + 7).to_s, roomtype_id: @rt.id,   hotel: @hotel)
        @rt.rates = [@rate1, @rate2, @rate3]
        @hotel.save
      end

      it 'should return the rate when the date requested is between rates' do
         result = Rate.resolve(@rt.id, Date.today + 2,  @hotel) 
         expect(result.id).to eq(@rate2.id)
      end 
      
      it "should correctly return the rate for the day that a rate is defined" do
         result = Rate.resolve(@rt.id, Date.today + 3,  @hotel)
         expect(result.id).to eq(@rate2.id)
      end
      
      it 'should return nil if the rate amount is nil is after the last entry' do
         result = Rate.resolve(@rt.id, Date.today + 8, @hotel)
         expect(result).to be(nil) 
      end   
      
      it "should ignore rates where active is not true" do
         FactoryGirl.create(:rate, date: Date.today + 5, roomtype_id: @rt.id, active: false, hotel: @hotel)
         result = Rate.resolve(@rt.id, Date.today + 4, @hotel)
         expect(result.id).to eq(@rate3.id)                 
      end
      
      it "should ignore rates for different room types" do
         @rt2 = FactoryGirl.create(:roomtype, hotel: @hotel)
         FactoryGirl.create(:rate, date: Date.today + 4, roomtype_id: @rt2.id, active: false, hotel: @hotel)
         result = Rate.resolve(@rt.id, Date.today + 4, @hotel)
         expect(result.id).to eq(@rate3.id)          
      end
    end
    
    describe '.total' do
      
      before(:each) do
        @hotel = FactoryGirl.create(:hotel)
        @rt = FactoryGirl.create(:roomtype, hotel: @hotel)
        @rate = FactoryGirl.create(:rate, date: Date.today.to_s, roomtype_id: @rt.id, hotel: @hotel)
        @tax_rate  = FactoryGirl.create(:tax_rate, rate: 0.03, hotel: @hotel) 
        @tax_rate2 = FactoryGirl.create(:tax_rate, rate: 0.05, hotel: @hotel) 
        @rate.tax_rate_ids = [@tax_rate.id, @tax_rate2.id]
      end
      
      it "should return the total cost for the rate" do
         result = @rate.total
         expect(result[:total_cents]).to eq(10800)
         expect(result[:total]).to eq("$108.00")
      end 
    
    end  
    
    
    ## VALIDATIONS ##
    describe 'validations' do
      
      before(:each) do
        @hotel = FactoryGirl.create(:hotel)
        @roomtype = FactoryGirl.create(:roomtype, hotel: @hotel)
        @rate = FactoryGirl.build(:rate, hotel: @hotel, roomtype_id: @roomtype.id)        
      end
      
      it "should create rates given valid information" do
         expect(@rate).to be_valid
      end



      it "should create rates for the same date but different roomtypes" do
         @rate.save!  
         rate2 = FactoryGirl.build(:rate, hotel: @hotel, roomtype_id: "rtpdiffers1")
         expect(rate2).to be_valid
     end
     
      it "should not create a rate when one exists with the same roomtype and date" do
         @rate.save!  
         rate2 = FactoryGirl.build(:rate, hotel: @hotel, roomtype_id: @roomtype.id)
         expect(rate2).to_not be_valid    
      end
    
      it "should not create a rate with an invalid date" do
         invalids = %w[1124768 11/05/2000 05/11/2000 11/11/2011 12/15/07 13/15/2015 12/41/2015 aa]
         invalids.each do | i |        
            rate = FactoryGirl.build(:rate, hotel: @hotel, date: i)
            expect(rate).to_not be_valid
         end
      end
 
    
    it "should not create a rate with an invalid price" do
         invalids = %w[11.24 1000000 aa $1000]
         invalids.each do | i |        
            rate = FactoryGirl.build(:rate, hotel: @hotel, rate_cents: i)
            expect(rate).to_not be_valid
         end      
    end
    
  #  it "should not create a rate with an invalid currency" do
  #      invalids = %w[1.1 1a aa]
  #       invalids.each do | i |        
  #        @rateset.each do | r |            
  #            r[:currency] = i
  #            Rate.new(r).should_not be_valid
  #        end 
  #    end
  # end
   
    
    it "should not create a rate with a blank date" do
            rate = FactoryGirl.build(:rate, hotel: @hotel)
            rate.date = nil
            expect(rate).to_not be_valid
    end
    
    it "should not create a rate with a blank rate_cents" do
            rate = FactoryGirl.build(:rate, hotel: @hotel)
            rate.rate_cents = nil
            expect(rate).to_not be_valid
    end
    
    it "should not create a rate with a blank currency" do
            rate = FactoryGirl.build(:rate, hotel: @hotel)
            rate.currency = nil
            expect(rate).to_not be_valid
    end
   

  end
end
