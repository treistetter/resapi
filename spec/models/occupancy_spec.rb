require 'rails_helper'

describe Occupancy do

      before(:each) do         
         @hotel = FactoryGirl.create(:hotel, create_roomtypes: true, create_rooms: true, create_rates: true, create_tax_rates: true)  
         @rt = @hotel.roomtypes[0]
         @room = @hotel.rooms[0]
         @tax_rate1 = FactoryGirl.create(:tax_rate, rate: 0.03, hotel: @hotel) 
         @tax_rate2 = FactoryGirl.create(:tax_rate, rate: 0.05, hotel: @hotel) 
         @params  =  {    date: (Date.today + 5).to_s,
                             guests: 1,
                            room_id: @room.id,                             
                     }       
         @occupancy = Occupancy.new(@params, @hotel)        
      end  

   
    
    describe '.calculate_cost' do
      
      before(:each) do
         
         @extra1 = FactoryGirl.create(:extra, price_cents: 1233, tax_rate_ids: [@tax_rate1.id, @tax_rate2.id], hotel: @hotel) 
         @extra2 = FactoryGirl.create(:extra, price_cents: 2435, tax_rate_ids: [@tax_rate1.id, @tax_rate2.id], hotel: @hotel) 
         @occupancy.extra_ids = [@extra1.id, @extra2.id]
      end
      
      it "should correclty add the extras cost details" do
          result = @occupancy.calculate_cost
          expect(result[:extras][:extras]).to eq([@extra1.calculate_cost, @extra2.calculate_cost])
      end
      
      it "should return the total cost" do
          result = @occupancy.calculate_cost
          expect(result[:total]).to eq("$69.24")          
      end
      
      it "should return the total cents cost" do
          result = @occupancy.calculate_cost
          expect(result[:total_cents]).to eq(6924)
      end
      
      it "should return the rate" do
          result = @occupancy.calculate_cost
          expect(result[:rate]).to eq("$27.43")
      end
      
      it "should return the rate cents" do
         result = @occupancy.calculate_cost
         expect(result[:rate_cents]).to eq(2743)
      end
      
      it "should return the gross of extras and rate" do
         result = @occupancy.calculate_cost
         expect(result[:gross]).to eq("$64.11")
      end
      
      it "should return the gross cents of extras and rate" do
         result = @occupancy.calculate_cost
         expect(result[:gross_cents]).to eq(6411)
      end
      
    end
    

      
    ## CLASS METHODS ##
    
    describe 'Occupancy.total_occupancies' do
      before(:each) do
         @extra1 = FactoryGirl.create(:extra, price_cents: 1253, tax_rate_ids: [@tax_rate1.id, @tax_rate2.id], hotel: @hotel)
         @extra2 = FactoryGirl.create(:extra, price_cents: 2355, tax_rate_ids: [@tax_rate1.id, @tax_rate2.id], hotel: @hotel) 
         @occupancy.extra_ids = [@extra1.id, @extra2.id]  
         @occ2 = FactoryGirl.create(:occupancy, rate: @rate, room_id: @room.id, date: Date.today + 6, hotel: @hotel)
         @occ3 = FactoryGirl.create(:occupancy, rate: @rate, room_id: @room.id, date: Date.today + 7, hotel: @hotel)
         @occs = [@occ2, @occupancy, @occ3]

         
      end
            
      it 'should return the total' do
         result = Occupancy.total_occupancies(@occs)
         expect(result[:total]).to eq("$127.84")
      end
      
      it 'should return the total cents' do
         result = Occupancy.total_occupancies(@occs)
         expect(result[:total_cents]).to eq(12784)
      end
      
      it 'should return the total taxes' do
         result = Occupancy.total_occupancies(@occs)
         expect(result[:total_taxes]).to eq("$9.47")
      end
      
      it 'should return the total cents tax' do
         result = Occupancy.total_occupancies(@occs)
         expect(result[:total_taxes_cents]).to eq(947)        
      end
      
      it 'should return the total gross extras' do
         result = Occupancy.total_occupancies(@occs)
         expect(result[:gross_extras]).to eq("$36.08")
      end
      
      it 'should return the total cents gross extras' do
         result = Occupancy.total_occupancies(@occs)
         expect(result[:gross_extras_cents]).to eq(3608)
      end
      
      it "should return the gross total of rates" do
          result = Occupancy.total_occupancies(@occs)
          expect(result[:gross_rate]).to eq("$82.29")
      end
      
      it "should return the gross total cents of rates" do
          result = Occupancy.total_occupancies(@occs)
          expect(result[:gross_rate_cents]).to eq(8229)
        
      end
      
      it 'should return the calculated cost for each occupancy' do
         result = Occupancy.total_occupancies(@occs)
         expect(result[:occupancies][0]).to eq(@occs[0].calculate_cost)
         expect(result[:occupancies][1]).to eq(@occs[1].calculate_cost)
         expect(result[:occupancies][2]).to eq(@occs[2].calculate_cost)
      end
      
    end
    
    
    ## VALIDATIONS ##


      describe 'validations' do    
        it "should create a new occupancy type given valid information" do
             expect(@occupancy).to be_valid
        end
        
     #   it "should not be valid if the date is not valid" do
     #     @params[:date] = "not a valid date"
     #     @occupancy = Occupancy.new(@params, @hotel)
     #     expect(@occupancy).to_not be_valid
     #   end
        
     #   it "should not create a new occupancy if the date inputted is before today" do
     #       @params[:date]= (Date.yesterday - 1).to_s
     #       @occupancy = Occupancy.new(@params, @hotel)  
     #       expect(@occupancy).to_not be_valid
     #   end
        
        it "should not create a new occupancy for the same room on the same date" do
            FactoryGirl.create(:reservation, occupancies: [@occupancy], hotel: @hotel)
            new_occ = Occupancy.new(@params, @hotel)
            expect(new_occ).to_not be_valid
        end
        
        
        it "should create a new occupancy for the same room on the same date if the old reservation is cancelled" do
            FactoryGirl.create(:reservation, occupancies: [@occupancy], hotel: @hotel, cancelled: true, cancelled_by_id: 1, cancelled_at: Time.now)
            new_occ = Occupancy.new(@params, @hotel)
            expect(new_occ).to be_valid
        end
                
        it " should not create an occupancy if the # of guests is nil" do
            @params[:guests] = nil
            @occupancy = Occupancy.new(@params, @hotel)
            expect(@occupancy).to_not be_valid  
        end
               
       it "should not be valid if the guests is greater than the max guests of the roomtype" do
            @params[:guests] = 15
            @occupancy = Occupancy.new(@params, @hotel)
            expect(@occupancy).to_not be_valid         
       end
        
   end 
end

