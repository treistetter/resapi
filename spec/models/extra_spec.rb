require 'rails_helper'

describe Extra do
  
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    @tax_rate  = FactoryGirl.create(:tax_rate, rate: 0.03, hotel: @hotel) 
    @tax_rate2 = FactoryGirl.create(:tax_rate, rate: 0.05, hotel: @hotel) 
    @extra_params = {price_cents: 1233, label: "Test Extra", description: "Test", active: true, reservation: false, tax_rate_ids: [@tax_rate.id, @tax_rate2.id]} 
    @extra = Extra.new(@extra_params, @hotel)
  end
  
  describe ' validations' do
    it "should be valid given valid params" do
      expect(@extra).to be_valid
    end
    
    it "should not be valid if the price_cents is null" do
      @extra_params[:price_cents] = nil
      test = Extra.new(@extra_params, @hotel)
      expect(test).to_not be_valid
    end
    
    it "should not be valid if hte price_cents is not an integer" do
      @extra_params[:price_cents] = "test"
      test = Extra.new(@extra_params, @hotel)
      expect(test).to_not be_valid      
    end
    
    it "should not be valid without a description" do
      @extra_params[:description] = nil
      test = Extra.new(@extra_params, @hotel)
      expect(test).to_not be_valid      
    end
    
    it "should not be valid if active is nil" do
      @extra_params[:active] = nil
      test = Extra.new(@extra_params, @hotel)
      expect(test).to_not be_valid      
    end
    
    it "should not be valid if reservation is nil" do
      @extra_params[:reservation] = nil
      test = Extra.new(@extra_params, @hotel)
      expect(test).to_not be_valid      
    end
    
    it "should not be valid if tax_rate_ids is nil" do
      @extra_params[:tax_rate_ids] = nil
      test = Extra.new(@extra_params, @hotel)
      expect(test).to_not be_valid      
    end
    
    it "should not be valid if tax_rate_ids is not an array" do
      @extra_params[:tax_rate_ids] = "test"
      test = Extra.new(@extra_params, @hotel)
      expect(test).to_not be_valid      
    end
    
  end
  
  
  describe '.details' do
    it "should return the label" do
      result = @extra.details
      expect(result[:label]).to eq(@extra_params[:label])
    end
      
    it "should return the id of the extra" do
      result = @extra.details
      expect(result[:id]).to eq(@extra.id)      
    end
    
    it "should return the description" do
      result = @extra.details
      expect(result[:description]).to eq(@extra_params[:description])
    end
    
    it "should return the pricing" do
      result = @extra.details
      expect(result[:pricing]).to eq(@extra.calculate_cost)      
    end
    
    it "should return active" do
      result = @extra.details
      expect(result[:active]).to eq(true)     
    end       
  end

  describe 'Extra.total_extras(extras)' do
    
    before(:each) do
      @extra2 = FactoryGirl.create(:extra, price_cents: 2435, tax_rate_ids: [@tax_rate.id, @tax_rate2.id], hotel: @hotel)           
      @extras = [@extra, @extra2]     
    end
    
    it "should return the total cost for all extras" do
        result = Extra.total_extras(@extras)
        expect(result[:total]).to eq("$39.62")
    end
    
    it "should return the total cost cents for all extras" do
        result = Extra.total_extras(@extras)     
        expect(result[:total_cents]).to eq(3962)  
    end
    
    it "should return the details for each extra" do
        result = Extra.total_extras(@extras)     
        expect(result[:extras]).to eq([@extra.calculate_cost, @extra2.calculate_cost]) 
    end
    
    it "should return the total taxes" do
        result = Extra.total_extras(@extras)      
        expect(result[:total_taxes]).to eq("$2.94")
    end
    
    it "should return the total taxes cents" do
        result = Extra.total_extras(@extras)      
        expect(result[:total_taxes_cents]).to eq(294)
    end
    
    it "should return the gross" do
        result = Extra.total_extras(@extras)
        expect(result[:gross]).to eq("$36.68")
    end
    
    it "should return the gross cents" do
        result = Extra.total_extras(@extras)
        expect(result[:gross_cents]).to eq(3668)
    end
       
  end
  
  describe '.calculate_cost' do
    it "should correclty calculate the total cost of the extra" do
       result = @extra.calculate_cost
       expect(result[:total_cents]).to eq(1332)
       expect(result[:total]).to eq("$13.32")
    end
    
    it "should return the total tax of the extra" do
      result = @extra.calculate_cost
      expect(result[:taxes][:total_cents]).to eq(99)
    end
    
    it "should return the cost of each taxes" do
      result = @extra.calculate_cost
      expect(result[:taxes][:rates][0][:amount_cents]).to eq(37)
      expect(result[:taxes][:rates][1][:amount_cents]).to eq(62)
    end
    
    it "should return the uuid of the extra" do
      result = @extra.calculate_cost
      expect(result[:id]).to eq(@extra.id)
    end
    
    it "should return the gross" do
      result = @extra.calculate_cost
      expect(result[:gross]).to eq("$12.33")
    end
    
    it "should return the gross cents" do
      result = @extra.calculate_cost
      expect(result[:gross_cents]).to eq(1233)
    end
    
  end 
end
