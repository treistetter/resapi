require 'rails_helper'

describe Reservation do
      before(:each) do      
            @hotel = FactoryGirl.create(:hotel, create_rooms: true, create_roomtypes: true, create_rates: true)
            @rt = @hotel.roomtypes[0]
            @room = @hotel.rooms[0]
            @extra = FactoryGirl.create(:extra, hotel: @hotel, reservation: true)
            @occ1 = FactoryGirl.build(:occupancy, room_id: @room.id, date: Date.today, hotel: @hotel)
            @occ2 = FactoryGirl.build(:occupancy, room_id: @room.id, date: Date.today + 1, hotel: @hotel) 
            @occ3 = FactoryGirl.build(:occupancy, room_id: @room.id, date: Date.today + 2, hotel: @hotel)
            @occs = [@occ1, @occ2, @occ3]
            @reservation_params = {
                      occupancies: @occs,
                            guest: FactoryGirl.build(:guest),
                         payments: [],
                        extra_ids: [@extra.id],
                        cancelled: false,
                  cancelled_by_id: nil,
                    cancel_reason: nil,
                     cancelled_at: nil,
                        finalized: true                           
            }      
            @reservation = Reservation.new(@reservation_params, @hotel)
       end
      
       it "should be valid given valid information" do
         test = Reservation.new(@reservation_params, @hotel)
         expect(test).to be_valid
       end
       
       it "should not be valid if there is no guest and finalized is true" do
         @reservation_params[:finalized] = true
         @reservation_params[:guest] = nil
         test = Reservation.new(@reservation_params, @hotel)
         expect(test).to_not be_valid         
       end
       
       it "should not be valid if the guest is not valid" do
         @reservation_params[:guest].first_name = nil
         test = Reservation.new(@reservation_params, @hotel)
         expect(test).to_not be_valid  
       end
       
       it "should not be valid if any of the extra_ids cant be found" do
         @reservation_params[:extra_ids] = ["invalid"]
         test = Reservation.new(@reservation_params, @hotel)
         expect(test).to_not be_valid            
       end
       
       it "should not be valid if any of hte extra_ids are not reservation extras" do
         extra = FactoryGirl.create(:extra, hotel: @hotel, reservation: false)
         @reservation_params[:extra_ids] = [extra.id]
         test = Reservation.new(@reservation_params, @hotel)
         expect(test).to_not be_valid
       end

       
       it "should not be valid if not cancelled and there are no occupancies" do
         @reservation_params[:occupancies] = []
         test = Reservation.new(@reservation_params, @hotel)
         expect(test).to_not be_valid                 
       end
       
       it "should not be valid if cancelled and there is no cancelled_by_id" do
         @reservation_params[:cancelled] = true
         @reservation_params[:cancelled_at] = Time.now
         @reservation_params[:cancel_reason] = "test test"
         test = Reservation.new(@reservation_params, @hotel)
         expect(test).to_not be_valid                 
       end
       
       
       it "should not be valid if cancelled and cancelled_at is blank" do
         @reservation_params[:cancelled] = true
         @reservation_params[:cancelled_by_id] = "user12345678"
         @reservation_params[:cancel_reason] = "test test"
         test = Reservation.new(@reservation_params, @hotel)
         expect(test).to_not be_valid          
       end
       
       it "should not be valid unless the occupancies are valid" do
         @occ1.guests = nil
         test = Reservation.new(@reservation_params, @hotel)
         expect(test).to_not be_valid  
       end
       
       it "should not be valid if there are duplicate occupancies" do
         @occ3.date = Date.today
         test = Reservation.new(@reservation_params, @hotel)
         expect(test).to_not be_valid           
       end
       
       describe '.delete_occupancies(ids)' do
         
         it "should remove the occupancies" do
            @reservation.delete_occupancies([@occ1.id, @occ3.id])
            expect(@reservation.occupancies).to eq([@occ2])
         end
         
         it "should return success true if valid" do
           expect(@reservation.delete_occupancies([@occ1.id, @occ3.id])).to eq(true)
         end
         
         it "should return success false if invalid IDs are passed in" do
           expect(@reservation.delete_occupancies(["invalid"])).to eq(false)           
         end
         
         it "should set the invalid ids into invalid_delete_occupancies" do
            @reservation.delete_occupancies(["invalid"])
            expect(@reservation.invalid_delete_occupancies).to eq(["invalid"])           
         end
         
         
       end
       
end
