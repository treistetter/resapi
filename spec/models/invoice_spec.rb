require 'rails_helper'

class TestInvoice 
  attr_accessor :occupancies, :delete_occupancies, :create_occupancies, :create_hold, :extras, :payments, :hotel
  include Invoice
end

describe Invoice do
  
  before(:each) do 
    @test = TestInvoice.new
  end
  
  describe '.invoice_with_payments' do
    before(:each) do
        @hotel = FactoryGirl.create(:hotel)
        roomtype_details = FactoryGirl.build(:roomtype, hotel: @hotel).details
        @hotel.create_roomtype(roomtype_details)
        @rt = @hotel.roomtypes[0]
        @room = FactoryGirl.build(:room, roomtype_id: @rt.id)
        @hotel.create_room(@room.details)
        r1 = FactoryGirl.create(:rate, rate_cents: 7142, hotel: @hotel)
        r2 = FactoryGirl.create(:rate, rate_cents: 8732, date: (Date.today + 1).to_s, hotel: @hotel)
        r3 = FactoryGirl.create(:rate, rate_cents: 9335, date: (Date.today + 2).to_s, hotel: @hotel)
        @occ1 = FactoryGirl.create(:occupancy, rate: r1, room_id: @room.id, hotel: @hotel)
        @occ2 = FactoryGirl.create(:occupancy, rate: r2, date: (Date.today + 2).to_s, room_id: @room.id, hotel: @hotel)
        @occ3 = FactoryGirl.create(:occupancy, rate: r3, date: (Date.today + 3).to_s, room_id: @room.id, hotel: @hotel) 
        @payment = Payment.create!({amount_cents: 11243, method: "authorize_and_capture", success: true}, @hotel)
        @payment2 = Payment.create!({amount_cents: 7132, method: "authorize_and_capture", success: true}, @hotel)
        @occs = [@occ1, @occ2, @occ3]
        @test.occupancies = @occs      
        @test.payments = [@payment, @payment2]        
    end
    
    it 'should return the amount paid' do
        result = @test.invoice_with_payments
        expect(result[:paid]).to eq("$183.75")      
    end
    
    it 'should return the amount paid in cents' do
        result = @test.invoice_with_payments
        expect(result[:paid_cents]).to eq(18375)
    end
    
    it 'should return the balance outstanding' do
        result = @test.invoice_with_payments
        expect(result[:balance_outstanding]).to eq("$68.34")
    end
    
    it 'should return the balance cents outstanding' do
       result = @test.invoice_with_payments
       expect(result[:balance_outstanding_cents]).to eq(6834)
    end
    
    it 'should return details of the payments' do  
       result = @test.invoice_with_payments
       expect(result[:payments]).to eq([@payment.details, @payment2.details])      
    end
    
  end
  
  describe '.invoice_with_deposit' do
    
    before(:each) do
       @hotel = FactoryGirl.create(:hotel)
    end
    
    it "should correctly return the deposit_due_cents" do       
       allow(@test).to receive(:invoice) {{total_cents: 17100}}
       @hotel.deposit_rate = 0.35
       allow(@test).to receive(:hotel) {@hotel}
       result = @test.invoice_with_deposit
       expect(result[:deposit_due_cents]).to eq(5985)
    end
    
    it "should correctly return the deposit_due" do
       allow(@test).to receive(:invoice) {{total_cents: 17100}}
       @hotel.deposit_rate = 0.35
       allow(@test).to receive(:hotel) {@hotel}
       result = @test.invoice_with_deposit
       expect(result[:deposit_due]).to eq("$59.85")      
    end
    
    it "should return zero for the deposit_due if the hotel does not have a rate set" do
       allow(@test).to receive(:invoice) {{total_cents: 17100}}
       allow(@test).to receive(:hotel) {@hotel}
       result = @test.invoice_with_deposit
       expect(result[:deposit_due]).to eq("$0.00")      
    end
    
    it "should return zero for the deposit_due_cents if the hotel does not have a rate set" do
       allow(@test).to receive(:invoice) {{total_cents: 17100}}
       allow(@test).to receive(:hotel) {@hotel}
       result = @test.invoice_with_deposit
       expect(result[:deposit_due_cents]).to eq(0)            
    end
  end
  
  describe '.invoice' do

     before(:each) do
        @hotel = FactoryGirl.create(:hotel)
        roomtype_details = FactoryGirl.build(:roomtype, hotel: @hotel).details
        @hotel.create_roomtype(roomtype_details)
        @rt = @hotel.roomtypes[0]
        @room = FactoryGirl.build(:room, roomtype_id: @rt.id)
        @hotel.create_room(@room.details)
        tr1 = FactoryGirl.create(:tax_rate, rate: 0.03, hotel: @hotel)
        tr2 = FactoryGirl.create(:tax_rate, rate: 0.05, hotel: @hotel)
        r1 = FactoryGirl.create(:rate, rate_cents: 7142, hotel: @hotel, tax_rate_ids: [tr1.id, tr2.id])
        r2 = FactoryGirl.create(:rate, rate_cents: 8732, date: (Date.today + 1).to_s, hotel: @hotel, tax_rate_ids: [tr1.id, tr2.id])
        r3 = FactoryGirl.create(:rate, rate_cents: 9335, date: (Date.today + 2).to_s, hotel: @hotel, tax_rate_ids: [tr1.id, tr2.id])
        @extra = FactoryGirl.create(:extra, price_cents: 1153, tax_rate_ids: [tr2.id], hotel: @hotel)      
        @occ1 = FactoryGirl.create(:occupancy, rate: r1, room_id: @room.id, hotel: @hotel)
        @occ2 = FactoryGirl.create(:occupancy, rate: r2, date: (Date.today + 2).to_s, room_id: @room.id, hotel: @hotel, extra_ids: [@extra.id])
        @occ3 = FactoryGirl.create(:occupancy, rate: r3, date: (Date.today + 3).to_s, room_id: @room.id, hotel: @hotel) 
        @occs = [@occ1, @occ2, @occ3]
        @test.occupancies = @occs   
        @test.extras = [@extra]
     end


    it 'should return the total' do
        result = @test.invoice
        expect(result[:total]).to eq("$296.48")
    end
    
    it 'should return the total cents amount' do
        result = @test.invoice
        expect(result[:total_cents]).to eq(29648)
    end
    
    it 'should return the total taxes' do
        result = @test.invoice
        expect(result[:total_taxes]).to eq("$21.33")
    end
    
    it 'should return the total cents taxes' do
        result = @test.invoice
        expect(result[:total_taxes_cents]).to eq(2133)
    end
    
    it "should return the gross amount" do
        result = @test.invoice
        expect(result[:gross]).to eq("$275.15")
    end   

    it "should return the gross cents amount" do
        result = @test.invoice
        expect(result[:gross_cents]).to eq(27515)
    end
      
    it 'should return detail for the reservation extras' do
        result = @test.invoice
        expect(result[:reservation_extras]).to eq(Extra.total_extras([@extra]))
    end

    
    it 'should return detail for each occupancy' do
        result = @test.invoice
        expect(result[:occupancy]).to eq(Occupancy.total_occupancies(@occs))     
    end    
  end
  

  
  describe '.occupancy_line_items' do
     before(:each) do
        @hotel = FactoryGirl.create(:hotel)
        roomtype_details = FactoryGirl.build(:roomtype, hotel: @hotel).details
        @hotel.create_roomtype(roomtype_details)
        @rt = @hotel.roomtypes[0]
        @room = FactoryGirl.build(:room, roomtype_id: @rt.id)
        @hotel.create_room(@room.details)
        r1 = FactoryGirl.create(:rate, rate_cents: 7142, hotel: @hotel)
        r2 = FactoryGirl.create(:rate, rate_cents: 8732, date: (Date.today + 1).to_s, hotel: @hotel)
        r3 = FactoryGirl.create(:rate, rate_cents: 9335, date: (Date.today + 2).to_s, hotel: @hotel)
        @occ1 = FactoryGirl.create(:occupancy, rate: r1, room_id: @room.id, hotel: @hotel)
        @occ2 = FactoryGirl.create(:occupancy, rate: r2, date: (Date.today + 2).to_s, room_id: @room.id, hotel: @hotel)
        @occ3 = FactoryGirl.create(:occupancy, rate: r3, date: (Date.today + 3).to_s, room_id: @room.id, hotel: @hotel) 
        @payment = Payment.create!({amount_cents: 11243, method: "authorize_and_capture", success: true}, @hotel)
        @payment2 = Payment.create!({amount_cents: 7132, method: "authorize_and_capture", success: true}, @hotel)
        @occs = [@occ1, @occ2, @occ3]
        @test.occupancies = @occs      
        @test.payments = [@payment, @payment2] 
     end
     
     it 'should return an array of occupancies details' do
        result = @test.occupancy_line_items
        result.each_with_index do |occ, i|
          expect(occ).to eq(@occs[i].details)
        end
     end
     

     
  end
  
  describe '.room_objects' do
    it "should return unique rooms" do
      @hotel = FactoryGirl.create(:hotel)
      @test.hotel = @hotel
      roomtype_details = FactoryGirl.build(:roomtype, hotel: @hotel).details
      @hotel.create_roomtype(roomtype_details)
      @rt = @hotel.roomtypes[0]
      room1 = FactoryGirl.build(:room, roomtype_id: @rt.id)
      room2 = FactoryGirl.build(:room, roomtype_id: @rt.id)
      @hotel.create_room(room1.details)
      @hotel.create_room(room2.details)
      occ1 = Occupancy.new({room_id: room1.id}, @hotel)
      occ2 = Occupancy.new({room_id: room1.id}, @hotel)
      occ3 = Occupancy.new({room_id: room2.id}, @hotel)
      allow(@test).to receive(:occupancies) {[occ1, occ2, occ3]}
      expect(@test.room_objects[0].id).to eq(room1.id)
      expect(@test.room_objects[1].id).to eq(room2.id)
    end
  end
  
  describe '.stays' do
    before(:each) do
        @hotel = FactoryGirl.create(:hotel)
        @test.hotel = @hotel
        roomtype_details = FactoryGirl.build(:roomtype, hotel: @hotel).details
        @hotel.create_roomtype!(roomtype_details)
        @rt = @hotel.roomtypes[0]
        @room1 = FactoryGirl.build(:room, hotel: @hotel, roomtype_id: @rt.id)    
        @hotel.create_room!(@room1.details)
        @occ1 = Occupancy.new({room_id: @room1.id, date: Date.today, guests: 1}, @hotel)
        @occ2 = Occupancy.new({room_id: @room1.id, date: Date.today + 1, guests: 1}, @hotel)
        @occ3 = Occupancy.new({room_id: @room1.id, date: Date.today + 2, guests: 1}, @hotel)
        @occs = [@occ1, @occ2, @occ3]

    end
    
    it "should return the correct check in and check out for a single room" do
       allow(@test).to receive(:occupancies) {@occs}
       result = @test.stays
       expect(result[0]).to eq({room: @room1.id, check_in: Date.today.to_s, check_out: (Date.today + 3).to_s, guests: 1}) # checkout should be one day after last reserved night
    end
    
    it "should return the correct check in and check outs for multiple rooms" do
      
       room2 = FactoryGirl.build(:room, roomtype_id: @rt.id, hotel: @hotel)
       @hotel.create_room(room2.details, @hotel)
       occ1 = Occupancy.new({room_id: room2.id, date: Date.today, guests: 1}, @hotel)
       occ2 = Occupancy.new({room_id: room2.id, date: Date.today + 1, guests: 1}, @hotel)
       occ3 = Occupancy.new({room_id: room2.id, date: Date.today + 2, guests: 1}, @hotel)
       occ4 = Occupancy.new({room_id: room2.id, date: Date.today + 3, guests: 1}, @hotel)
       occs = @occs + [occ1, occ2, occ3, occ4]
       allow(@test).to receive(:occupancies) {occs}
       result = @test.stays
       expect(result[0]).to eq({room: @room1.id, check_in: Date.today.to_s, check_out: (Date.today + 3).to_s, guests: 1}) # checkout should be one day after last reserved night
       expect(result[1]).to eq({room: room2.id, check_in: Date.today.to_s, check_out: (Date.today + 4).to_s, guests: 1}) # checkout should be one day after last reserved night      
    end
    
    it "should correctly handle a stay with a single occupancy" do
       allow(@test).to receive(:occupancies) {[@occ2]}
       result = @test.stays
       expect(result[0]).to eq({room: @room1.id, check_in: (Date.today +  1).to_s, check_out: (Date.today + 2).to_s, guests: 1})      
    end
    
    it "should correctly handle multiple check ins and checkouts for a single room" do
      occ4 = Occupancy.new({room_id: @room1.id, date: Date.today + 4, guests: 1}, @hotel)
      occ5 = Occupancy.new({room_id: @room1.id, date: Date.today + 5, guests: 1}, @hotel)
      occs = @occs + [occ4, occ5]
      allow(@test).to receive(:occupancies) {occs}
      result = @test.stays
      expect(result[0]).to eq({room: @room1.id, check_in: Date.today.to_s, check_out: (Date.today + 3).to_s, guests: 1})
      expect(result[1]).to eq({room: @room1.id, check_in: (Date.today + 4).to_s, check_out: (Date.today + 6).to_s, guests: 1})
    end
    
    it "should correctly handle multiple check ins and check outs for a single room with a single night occupancy at the end" do
      occ4 = Occupancy.new({room_id: @room1.id, date: Date.today + 4, guests: 1}, @hotel)
      occs = @occs + [occ4]
      allow(@test).to receive(:occupancies) {occs}
      result = @test.stays
      expect(result[0]).to eq({room: @room1.id, check_in: Date.today.to_s, check_out: (Date.today + 3).to_s, guests: 1})
      expect(result[1]).to eq({room: @room1.id, check_in: (Date.today + 4).to_s, check_out: (Date.today + 5).to_s, guests: 1})      
    end
    
    it "should handle multiple check ins and check outs with a single night occupancy at the beginning" do
       occ1 = Occupancy.new({room_id: @room1.id, date: Date.today, guests: 1}, @hotel)
       occ2 = Occupancy.new({room_id: @room1.id, date: Date.today + 2, guests: 1}, @hotel)
       occ3 = Occupancy.new({room_id: @room1.id, date: Date.today + 3, guests: 1}, @hotel)
       allow(@test).to receive(:occupancies) {[occ1, occ2, occ3]}
       result = @test.stays
       expect(result[0]).to eq({room: @room1.id, check_in: Date.today.to_s, check_out: (Date.today + 1).to_s, guests: 1})
       expect(result[1]).to eq({room: @room1.id, check_in: (Date.today + 2).to_s, check_out: (Date.today + 4).to_s, guests: 1})         
  
    end
    
    it "should handle multiple check ins and check outs with the single occupancy sandwiched" do
       occ1 = Occupancy.new({room_id: @room1.id, date: Date.today, guests: 1}, @hotel)
       occ2 = Occupancy.new({room_id: @room1.id, date: Date.today + 1, guests: 1}, @hotel)
       occ3 = Occupancy.new({room_id: @room1.id, date: Date.today + 3, guests: 1}, @hotel)
       occ4 = Occupancy.new({room_id: @room1.id, date: Date.today + 5, guests: 1}, @hotel)
       occ5 = Occupancy.new({room_id: @room1.id, date: Date.today + 6, guests: 1}, @hotel)
       allow(@test).to receive(:occupancies) {[occ1, occ2, occ3, occ4, occ5]}
       result = @test.stays
       expect(result[0]).to eq({room: @room1.id, check_in: Date.today.to_s, check_out: (Date.today + 2).to_s, guests: 1})
       expect(result[1]).to eq({room: @room1.id, check_in: (Date.today + 3).to_s, check_out: (Date.today + 4).to_s, guests: 1}) 
       expect(result[2]).to eq({room: @room1.id, check_in: (Date.today + 5).to_s, check_out: (Date.today + 7).to_s, guests: 1})     
    end
    
    it "should handle multiple single occupancies and a longer occupancy" do
       occ1 = Occupancy.new({room_id: @room1.id, date: Date.today, guests: 1}, @hotel)
       occ2 = Occupancy.new({room_id: @room1.id, date: Date.today + 2, guests: 1}, @hotel)
       occ3 = Occupancy.new({room_id: @room1.id, date: Date.today + 3, guests: 1}, @hotel)
       occ4 = Occupancy.new({room_id: @room1.id, date: Date.today + 5, guests: 1}, @hotel)
       allow(@test).to receive(:occupancies) {[occ1, occ2, occ3, occ4]}
       result = @test.stays
       expect(result[0]).to eq({room: @room1.id, check_in: Date.today.to_s, check_out: (Date.today + 1).to_s, guests: 1})
       expect(result[1]).to eq({room: @room1.id, check_in: (Date.today + 2).to_s, check_out: (Date.today + 4).to_s, guests: 1}) 
       expect(result[2]).to eq({room: @room1.id, check_in: (Date.today + 5).to_s, check_out: (Date.today + 6).to_s, guests: 1})         
    end
    
  end
  
end