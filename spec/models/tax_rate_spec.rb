require 'rails_helper'

describe TaxRate do
  
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @tax_rate_params = {    rate: 0.15,
                       description: "Description",
                            active: true,
                              name: "Test Rate"
        }
    end


    
    it 'should not be valid if the rate is blank' do
        @tax_rate_params[:rate] = nil
        test = TaxRate.new(@tax_rate_params, @hotel)
        expect(test).to_not be_valid
    end
    
    it 'should not be valid if the rate is greater or equal to 1' do
        @tax_rate_params[:rate] = 1
        test = TaxRate.new(@tax_rate_params, @hotel)
        expect(test).to_not be_valid      
    end
    
    it 'should not be valid if the rate is 0 or less' do
        @tax_rate_params[:rate] = 0
        test = TaxRate.new(@tax_rate_params, @hotel)
        expect(test).to_not be_valid      
    end
    
    it 'should not be valid if the rate is not a number' do
        @tax_rate_params[:rate] = "test"
        test = TaxRate.new(@tax_rate_params, @hotel)
        expect(test).to_not be_valid      
    end
    
    it 'should not be valid if the description is blank' do
        @tax_rate_params[:description] = nil
        test = TaxRate.new(@tax_rate_params, @hotel)
        expect(test).to_not be_valid      
    end
    

    
    it 'should not be valid if the name is blank' do
        @tax_rate_params[:name] = nil
        test = TaxRate.new(@tax_rate_params, @hotel)
        expect(test).to_not be_valid      
    end
   
    
    
    describe '.calculate_amount(gross)' do
      it "should return a hash of the dollar amount and tax_rate id and round down" do
        tr = @hotel.create_tax_rate(name: "Test", description: "Test test test", rate: 0.07, active: true)
        result = tr.calculate_amount(Money.new(13532))
        expect(result[:amount_cents]).to eq(947)
        expect(result[:id]).to eq(tr.id)
      end
      
       it "should return a hash of the dollar amount and tax_rate id and round up" do
        tr = @hotel.create_tax_rate(name: "Test", description: "Test test test", rate: 0.07, active: true)
        result = tr.calculate_amount(Money.new(13536))
        expect(result[:amount_cents]).to eq(948)
        expect(result[:id]).to eq(tr.id)
      end     
    end 
    
    describe 'class method calculate_total' do
      it 'should take an array of calculated amounts and return the total' do
          tr1 = @hotel.create_tax_rate(name: "Test", description: "Test test test", rate: 0.02, active: true)
          tr2 = @hotel.create_tax_rate(name: "Test", description: "Test test test", rate: 0.03, active: true)
          tr3 = @hotel.create_tax_rate(name: "Test", description: "Test test test", rate: 0.07, active: true)
          
          amounts = []
          amounts << tr1.calculate_amount(Money.new(13532))
          amounts << tr2.calculate_amount(Money.new(13532))
          amounts << tr3.calculate_amount(Money.new(13532))
          
          result = TaxRate.calculate_total(amounts)
          expect(result[:total_cents]).to eq(1624)
          result[:rates].each_with_index do | rate , i|
            expect(rate[:id]).to eq(amounts[i][:id])
          end
          
      end
    end
end