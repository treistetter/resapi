require 'rails_helper'

describe Guest do
  before(:each) do
     @hotel = FactoryGirl.create(:hotel)
     @address = {line1: "Test Street",
                 line2: "Test Apartment",
                  city: "Atlanta",
                region: "GA",
           postal_code: "30306"
       }
     @guest = {
       first_name: "John",
        last_name: "Doe", 
            email: "test@test.com",
            phone: "1234567890",
  phone_secondary: "2344634644",
          address: @address 
       
       
     }
  end
  
  it "should create a guest given valid information" do
      test_guest = Guest.new(@guest)
      expect(test_guest).to be_valid
  end
  
  it "should accept multiple formats for phone and store as integers" do
       numbers = ["(847) 856 1343", "(847) 856-1343", "847-856-1343", "847 856 1343"]
       numbers.each do |number|
         @guest[:phone] = number
         test_guest = Guest.new(@guest)
         expect(test_guest.details[:phone]).to eq("8478561343")
       end
  end       
          
  it "should accept multiple formats for the secondary phone and store a integers" do
      numbers = ["(847) 856 1343", "(847) 856-1343", "847-856-1343", "847 856 1343"]
      numbers.each do |number|
        @guest[:phone_secondary] = number
        test_guest = Guest.new(@guest)
        expect(test_guest.details[:phone_secondary]).to eq("8478561343")
      end         
  end
  
  it "should not create a guest without a first name" do    
     @guest[:first_name] = ""
     test_guest = Guest.new(@guest)
     expect(test_guest).to_not be_valid    
  end
  
  it "should not create a guest without a last name" do
    
     @guest[:last_name] = ""
     test_guest = Guest.new(@guest)
     expect(test_guest).to_not be_valid    
  end
 
  
  it "should not create a guest given an invalid email" do  
     addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
     addresses.each do |address|
       @guest[:email] = address
       test_guest = Guest.new(@guest)
       expect(test_guest).to_not be_valid  
     end   
  end
  
  it "should create the address as an instance of Address" do
    test_guest = Guest.new(@guest)
    expect(test_guest.address.class).to eq(Address)       
  end 
end
