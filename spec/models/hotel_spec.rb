require 'rails_helper'

describe Hotel do
  
  before(:each) do
    @params = {name: "Test Hotel",
              email: "test@testhotel.com",
              phone: "1232342432",
             phone2: "2131234422" 
              }
  end
  
  describe "Hotel" do
    
    it "should be valid given valid parameters" do
      hotel = Hotel.new(@params)
      expect(hotel).to be_valid
    end
    
    it "should not be valid if the name is blank" do
      @params[:name] = nil
      hotel = Hotel.new(@params)
      expect(hotel).to_not be_valid
    end
    
    it "should not be valid if the name is too long" do
      @params[:name] = "a" * 101
      hotel = Hotel.new(@params)
      expect(hotel).to_not be_valid      
    end
    
    it "should not be valid if the email is not valid" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
      addresses.each do |address|
        @params[:email] = address
        @hotel = Hotel.new(@params)
        expect(@hotel).to_not be_valid
      end      
    end
    
    it "should not be valid if the email is blank" do
      @params[:email] = nil
      hotel = Hotel.new(@params)
      expect(hotel).to_not be_valid      
    end
    
    it "should not be valid if the phone is too short" do
      @params[:phone] = 123456789
      hotel = Hotel.new(@params)
      expect(hotel).to_not be_valid      
    end
    
    it "should not be valid if the phone is too long" do
      @params[:phone] = 12345678901
      hotel = Hotel.new(@params)
      expect(hotel).to_not be_valid        
    end
    
    it "should not be valid if the phone has something other than numbers" do
      @params[:phone] = "123456789a"
      hotel = Hotel.new(@params)
      expect(hotel).to_not be_valid        
    end

    it "should not be valid if the phone2 is too short" do
      @params[:phone2] = 123456789
      hotel = Hotel.new(@params)
      expect(hotel).to_not be_valid      
    end
    
    it "should not be valid if the phone2 is too long" do
      @params[:phone2] = 12345678901
      hotel = Hotel.new(@params)
      expect(hotel).to_not be_valid        
    end
    
    it "should not be valid if the phone2 has something other than numbers" do
      @params[:phone2] = "123456789a"
      hotel = Hotel.new(@params)
      expect(hotel).to_not be_valid        
    end

    
  end
  
  describe "Hotel.create" do 
    @params = {name: "Test Hotel",
              email: "test@testhotel.com",
              phone: "1232342432",
             phone2: "2131234422" 
              }
                  
    it "should create the hotel db" do
       hotel = Hotel.create(@params)
       couch = Couch.new(Rails.application.secrets.couchdb)
       result = couch.get(hotel.id)
       expect(result[:db_name]).to eq(hotel.id)
    end
    
    it "should create the hotel's doc" do
       hotel = Hotel.create(@params)
       found = Hotel.find_by_id(hotel.id)
       expect(found.email).to eq(hotel.email)
    end
    
    it "should create the design doc" do
       hotel = Hotel.create(@params)
       couch = Couch.new(Rails.application.secrets.couchdb + hotel.id)
       result = couch.get("/_design/design")
       expect(result[:_id]).to eq("_design/design")      
    end
    
    it "should create the security doc" do
       hotel = Hotel.create(@params)
       couch = Couch.new(Rails.application.secrets.couchdb + hotel.id)
       result = couch.get("/_security")
       expect(result[:admins][:names].length).to eq(1)
       expect(result[:admins][:names][0]).to eq("server")
       expect(result[:admins][:roles].length).to eq(0)
       expect(result[:members][:roles].length).to eq(1)
       expect(result[:members][:roles][0]).to eq(hotel.id)
       expect(result[:members][:names].length).to eq(0)
    end
    
  end
  
  describe '.create_roomtype' do
    it "should persist a valid roomtype" do
      hotel = Hotel.create(@params)
      roomtype_params = {name: "Test Roomtype", description: "Test test", max_guests: 1, active: true}
      rt = hotel.create_roomtype(roomtype_params)
      hotel2 = Hotel.find_by_id(hotel.id)
      expect(hotel2.roomtypes.length).to eq(1)
      expect(hotel2.roomtypes[0].name).to eq("Test Roomtype")
    end
  end
  
  
  
end