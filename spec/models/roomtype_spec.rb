require 'rails_helper'

describe Roomtype do
  
  before(:each) do
    @params = {name: "Test Roomtype",
         max_guests: 2
              }
  end
  
  describe "Roomtype" do
    
    it "should be valid given valid parameters" do
      rt = Roomtype.new(@params)
      expect(rt).to be_valid
    end
    
    it "should not be valid if the name is blank" do
      @params[:name] = nil
      rt = Roomtype.new(@params)
      expect(rt).to_not be_valid
    end
    
    it "should not be valid if the name is too long" do
      @params[:name] = "a" * 31
      rt = Roomtype.new(@params)
      expect(rt).to_not be_valid      
    end


    it "should not be valid if the name is duplicated" do
      hotel = Object.new
      allow(hotel).to receive(:roomtypes) {[Roomtype.new(name: "Test Roomtype")]}
      rt = Roomtype.new(@params)
      rt.hotel = hotel
      expect(rt).to_not be_valid  
    end
    
    it "should not be valid if the max guests is not valid" do
      tests = %w[@ i]
      tests.each do |mg|
        @params[:max_guests] = mg
        @rt = Roomtype.new(@params)
        expect(@rt).to_not be_valid
      end      
    end
    
    it "should not be valid if the max_guests is blank" do
      @params[:max_guests] = nil
      rt = Roomtype.new(@params)
      expect(rt).to_not be_valid      
    end
    
     it "should not be valid if the max_guests is 0" do
      @params[:max_guests] = 0
      rt = Roomtype.new(@params)
      expect(rt).to_not be_valid      
    end
    
     it "should not be valid if the max_guests is more than 10" do
      @params[:max_guests] = 11
      rt = Roomtype.new(@params)
      expect(rt).to_not be_valid      
    end    
  end
  
 
  
  
end