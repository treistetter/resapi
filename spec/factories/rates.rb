FactoryGirl.define do
  factory :rate do
     date Date.today.to_s
     currency "USD"
     active true
     rate_cents 10000
     roomtype_id "roomtype"
     
     ignore do
       hotel nil
     end
     
     initialize_with { Rate.new({rate_cents: rate_cents}, hotel) }
  end

end
