class Card
  
   def self.generate(guest = nil)
    
     
    number = "4111111111111111"
    verification_value = "311"
    month = (Date.today + 60).month.to_s
    year = (Date.today + 60).year.to_s
    guest.nil? ? first_name = "Tester" : first_name = guest[:first_name]
    guest.nil? ? last_name = "McGee" : last_name = guest[:last_name]
    return {number: number, verification_value: verification_value, month: month, year: year, first_name: first_name, last_name: last_name}
   end
   
   def self.general_error
    number = "4222222222222"
    verification_value = "311"
    month = (Date.today + 60).month.to_s
    year = (Date.today + 60).year.to_s
   first_name = "Tester" 
    last_name = "McGee"
    return {number: number, verification_value: verification_value, month: month, year: year, first_name: first_name, last_name: last_name}
     
   end
   
   def self.general_error_address
     address1 = "123 Main street"
     zip = "46282"
     state = "GA"
     city = "Alpharetta"
     country = "US"

     return Address.new({
      line1: address1,
      city: city,
      region: state,
      country: country,
      postal_code: zip
      })
     
   end
   
   def self.generate_billing_address(guest = nil)
     address1 = "123 Main street"
     zip = "60031"
     state = "GA"
     city = "Alpharetta"
     phone = "123 456 7890"
     country = "US"
     guest.nil? ? name = "Tester McGee" : guest.full_name
     return guest.billing_address if guest
     return {name: name,
      address1: address1,
      city: city,
      state: state,
      country: country,
      zip: zip,
      phone: phone}
     
   end
   
   
end

