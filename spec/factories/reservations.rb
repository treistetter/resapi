FactoryGirl.define do
  factory :reservation do

    
     guest FactoryGirl.build(:guest)
     finalized true
     cancelled false
     cancelled_at nil
     cancelled_by_id nil
     cancel_reason nil
     payments []
     occupancies []

     ignore do
       hotel nil
     end


     
     initialize_with { Reservation.new({}, hotel) }
  end

end
