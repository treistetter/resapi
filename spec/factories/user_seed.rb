class UserSeed
  
  @@database = nil
  
  def self.reset_database
    self.set_database(Rails.application.secrets.couchdb)
    delete_database
    generate_database
  end
  
  def self.delete_database
   # begin
     #self.database.delete("_users")
    #rescue
   # end  
  end
  
  def self.generate_database

    # RestClient.put(self.database_url, "")
    
    views = {}    
    by_email_map = 'function(doc){emit(doc.email, null);}'
    views["by_email"] = {}
    views["by_email"]["map"] = by_email_map 

    by_username = 'function(doc){emit(doc.name, null);}'
    views["by_username"] = {}
    views["by_username"]["map"] = by_username 
    begin
      self.database.put("_users/_design/design", {views: views}) # create design document    
    rescue RestClient::Conflict => e   
    end
  end
  
  def self.database
    @@database
  end
  
  def self.set_database(url)
    @@database = Couch.new(url)
  end
  
  
end