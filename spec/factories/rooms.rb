FactoryGirl.define do
  factory :room do
     sequence(:name) {|n| "Test Room #{n}#{rand(1000)}" }
     roomtype_id 1
  
     ignore do
       hotel nil
     end
    
    initialize_with { Room.new({}, hotel) }
  end
end    