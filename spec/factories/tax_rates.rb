FactoryGirl.define do
  factory :tax_rate do
    sequence(:name) {|n| "Test Rate #{n}#{rand(1000)}" }

    description 'Description of test rate'
    active true
    rate 0.05
    ignore do 
      hotel nil
    end
    
    initialize_with { TaxRate.new({}, hotel) }
  end
end    
