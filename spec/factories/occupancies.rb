# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :occupancy do
    date Date.today
    room_id 1
    guests 1
    rate_id nil
    
    ignore do
      hotel
    end
    
    initialize_with { Occupancy.new({room_id: room_id}, hotel) }    
  end
end
