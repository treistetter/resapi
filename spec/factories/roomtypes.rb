FactoryGirl.define do
  factory :roomtype do
     sequence(:name) {|n| "Test Roomtype #{n}#{rand(1000)}" }
     max_guests 1
     description Forgery(:lorem_ipsum).sentences(6)
     
     ignore do
       hotel nil
     end
    
    initialize_with { Roomtype.new({}, hotel) }
  end
end    