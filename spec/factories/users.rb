FactoryGirl.define do
  factory :user do
    first_name 'Test'
    last_name 'User'
    sequence(:username) {|n| "tester#{n}#{rand(1000)}" }
    sequence(:email) {|n| "email#{n}#{rand(1000)}@factory2.com" }
    type 'user'
    password 'password'
    hotel_id "hot12345678"
    access_level 'owner'
    
    ignore do
      hotel nil
    end
    
    initialize_with { User.new({}, hotel) }
  end
end    