FactoryGirl.define do
  factory :hotel do
    sequence(:name) {|n| "Test Hotel #{n}#{rand(1000)}" }
    sequence(:email) {|n| "email#{n}#{rand(1000)}@testhotel.com" }
    phone 1234567890
    phone2 1234567890
    domain "testbnb.com"
    outgoing_email "noreply@testbnb.com"
    deposit_rate nil
    booking true
    cc_sandbox true
    cc_authorize false
    cancel_policy [{days: 3, percent: "0.95"}, {days: 7, percent: "0.75"}, {days: 14, percent: "0.5"}, {days: 30, percent: "0.25"}]
    initialize_with { Hotel.new({}) }
    
    ignore do
      create_roomtypes false
      roomtype_count 1
      create_rooms false
      room_count 1
      create_rates false
      create_extras false
      extra_count 1
      create_tax_rates false
      tax_rate_count 1      
    end
  
    after(:build) do |hotel, evaluator|    
      if evaluator.create_roomtypes
        evaluator.roomtype_count.times do |i|
          rt = FactoryGirl.build(:roomtype, hotel: hotel, max_guests: 2)
          hotel.create_roomtype!(rt.details)
        end
      end
      
      
      if evaluator.create_rooms
        evaluator.room_count.times do |i|
          hotel.roomtypes.each do |rt|
            rm = FactoryGirl.build(:room, roomtype_id: rt.id, hotel: hotel)
            hotel.create_room!(rm.details)
          end
        end      
      end
      
      trs = []
      
      if evaluator.create_tax_rates
        evaluator.tax_rate_count.times do |i|
           tr = FactoryGirl.build(:tax_rate, rate: 0.03, hotel: hotel)
           trs << hotel.create_tax_rate!(tr.details).id
           tr = FactoryGirl.build(:tax_rate, rate: 0.05, hotel: hotel)
           trs << hotel.create_tax_rate!(tr.details).id
        end
      end
      
      if evaluator.create_extras
        evaluator.extra_count.times do |i|
          extra = FactoryGirl.build(:extra, hotel: hotel, tax_rate_ids: trs)
          hotel.create_extra(extra.details)
          res_extra = FactoryGirl.build(:extra, hotel: hotel, reservation: true, tax_rate_ids: trs)
          hotel.create_extra(res_extra.details)
        end
      end
      
      if evaluator.create_rates
        
          rates = {}
          hotel.roomtypes.each_with_index do |rt, i|
            rt.rates.push(FactoryGirl.build(:rate, rate_cents: 2743, tax_rate_ids: trs, hotel: hotel, date: Date.today + 30, roomtype_id: rt.id))
            4.times do |j|
             z = j + 1
             adjustment = (1 + i/8.0) + (1 + z/11.0)
             rate = (adjustment * 4000).to_i
             rt.rates.push(FactoryGirl.build(:rate, rate_cents: rate, tax_rate_ids: trs, hotel: hotel, date: Date.today + 30 * z, roomtype_id: rt.id))
           end                   
         end
         hotel.save!          
      end
      
             
    end    
  end
end    