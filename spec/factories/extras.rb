# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :extra do
    price_cents 1234
    sequence(:label) { |n| "Extra #{n}"}
    sequence(:description){ |n| "Extra #{n} description"}
    active true
    tax_rate_ids []
    reservation false
    
    ignore do 
      hotel nil
    end
    
    initialize_with { Extra.new({}, hotel) }    
  end
end
