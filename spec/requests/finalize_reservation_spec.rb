require 'rails_helper'

describe FinalizeReservation do
   before(:each) do
      @hotel = FactoryGirl.create(:hotel, create_roomtypes: true, create_rooms: true, create_rates: true)
      @rt = @hotel.roomtypes[0]
      @room = @hotel.rooms[0]
      @extra = FactoryGirl.create(:extra, hotel: @hotel, reservation: false)
      @res_extra = FactoryGirl.create(:extra, hotel: @hotel, reservation: true)
      @occ1 = {room_id: @room.id, date: Date.today + 1, guests: 1, extra_ids: [@extra.id]}
      @occ2 = {room_id: @room.id, date: Date.today + 2, guests: 1, extra_ids: [@extra.id]}
      @occ3 = {room_id: @room.id, date: Date.today + 3, guests: 1, extra_ids: [@extra.id]}         
      @occupancies = [@occ1, @occ2, @occ3]
      @extras = [@res_extra.id]
      @params = {      occupancies: @occupancies,
                         extra_ids: @extras,
                         cancelled: false,
                         finalized: false
        } 
 
      @reservation = Reservation.create!(@params, @hotel)
      
      @guest_details = FactoryGirl.build(:guest, address: nil).details
      @billing_address = FactoryGirl.build(:address).details
      @card = Card.generate(@guest_details)
      
      @request_params = {guest: @guest_details, billing_address: @billing_address, card: @card}
      @request = FinalizeReservation.new(@request_params)
      @request.reservation = @reservation
      @request.hotel = @hotel
   end
   

   it "should return success true for a valid request" do
       result = @request.process
       expect(result[:success]).to eq(true)
   end   
   
   it "should set finalized to true on the reservation" do
       @request.process
       reservation = @hotel.find_reservation(@reservation.id)
       expect(reservation.finalized).to eq(true)
   end
        
   it "should store the hotel's cancellation policy on successful creation" do
       result = @request.process
       reservation = @hotel.find_reservation(result[:message][:id])
       expect(reservation.cancel_policy).to_not be_nil  
   end

   
   it "should create the reservation if the hotel's cancellation policy is nil" do
        @hotel.update_attributes(cancel_policy: nil)
        @request.hotel = @hotel
        result = @request.process
        expect(result[:success]).to eq(true)        
   end
     
   it "should not return success false if the guest information is blank" do
        @request_params[:guest] = nil
        test = FinalizeReservation.new(@request_params)
        test.hotel = @hotel
        test.reservation = @reservation
        result = test.process
        expect(result[:success]).to eq(false)
        expect(result[:errors][0][:code]).to eq("invalid_request")   
   end
   

   
   it "should return success false if guest information is invalid" do
        @request_params[:guest] = {phone:"1234567890", phone_secondary: "2345678901"}
        test = FinalizeReservation.new(@request_params)
        test.hotel = @hotel
        test.reservation = @reservation
        result = test.process
        expect(result[:success]).to eq(false)
        expect(result[:errors][0][:code]).to eq("invalid_request")
   end
   
   it "should return success false if a deposit is required and no payment information is supplied" do
        @hotel.update_attributes(deposit_rate: 0.10)       
        @request_params[:card] = nil
        test = FinalizeReservation.new(@request_params)
        test.hotel = @hotel
        test.reservation = @reservation
        result = test.process
        expect(result[:success]).to eq(false)
        expect(result[:errors][0][:code]).to eq("invalid_card")
   end
   
   
   it "should charge a deposit if the deposit_rate is set for the hotel" do
        @hotel.update_attributes(deposit_rate: 0.10)                  
        result = @request.process
        total_cost = result[:message][:invoice][:total_cents]
        deposit = (total_cost * @hotel.deposit_rate).to_i
        reservation = @hotel.find_reservation(@reservation.id)      
        expect(reservation.payments[0].amount_cents).to eq(deposit)     
   end
      
   
   it "should not charge a deposit/authorize the CC if the override matches what is stored for the hotel" do
        @request_params[:cc_override] = @hotel.cc_override
        test = FinalizeReservation.new(@request_params)
        test.hotel = @hotel
        test.reservation = @reservation
        result = test.process  
        reservation = @hotel.find_reservation(@reservation.id)
        expect(reservation.payments.length).to eq(0)
   end
   
   it "should return 0.00 for the amount charged if the override matches what is stored for the hotel" do
        @request_params[:cc_override] = @hotel.cc_override
        test = FinalizeReservation.new(@request_params)
        test.hotel = @hotel
        test.reservation = @reservation
        result = test.process  
        expect(result[:message][:invoice][:paid_cents]).to eq(0) 
   end
   
   it "should return a failure message if the deposit_rate is set for the hotel and payment fails" do
        @hotel.update_attributes(deposit_rate: 0.10)
        @request_params[:card] = Card.general_error
        @request_params[:billing_address] = Card.general_error_address.details
        test = FinalizeReservation.new(@request_params)
        test.hotel = @hotel
        test.reservation = @reservation
        result = test.process
        expect(result[:success]).to eq(false)
        expect(result[:errors][0][:code]).to eq("payment_failed")    
   end
   
   it "should not be valid if authorize_cc is true for the hotel and no payment information is supplied" do
        @hotel.update_attributes(cc_authorize: true)
        @request_params[:card] = nil
        test = FinalizeReservation.new(@request_params)
        test.hotel = @hotel
        test.reservation = @reservation
        result = test.process
        expect(result[:success]).to eq(false)
        expect(result[:errors][0][:code]).to eq("invalid_card")
   end 
   
   it "should not be valid if authorize_cc is true for the hotel and invalid payment information is supplied" do
        @hotel.update_attributes(cc_authorize: true)
        @request_params[:card][:number] = "not valid"
        test = FinalizeReservation.new(@request_params)
        test.hotel = @hotel
        test.reservation = @reservation
        result = test.process
        expect(result[:success]).to eq(false)
        expect(result[:errors][0][:code]).to eq("invalid_card")    
   end
   
   it "should make a 1 cent authorization on the cc if cc_authorize is true" do 
        @hotel.update_attributes(cc_authorize: true)
        result = @request.process
        reservation = @hotel.find_reservation(@reservation.id)      
        expect(reservation.payments[0].amount_cents).to eq(1)        
   end
   
     
   it "should send an e-mail to the customer on a successful creation" do
        test = FinalizeReservation.new(@request_params)
        test.hotel = @hotel
        test.reservation = @reservation
        result = test.process
        mail = ActionMailer::Base.deliveries.last
        mail['to'].to_s.should eq("tester@test.com")      
   end
     
   it "should not send an e-mail if the reservation fails" do
                @hotel = Hotel.last
                @hotel.update_attributes!(cc_authorize: true)
                @hotel = Hotel.last
                @request_hash[:card][:number] = "1" * 16
                @request_string = @request_hash.to_json
                @request = {request: @request_string, hotel_id: @hotel.id, user_id: @user.id, ipaddress: "127.0.0.1"}
                test = FinalizeReservation.new(@request)
                expect { test.process(@hotel) }.not_to change(ActionMailer::Base.deliveries, :count) 
   end
end