require 'rails_helper'

describe ReservationRequest do
     before(:each) do
        @hotel = FactoryGirl.create(:hotel, create_roomtypes: true, create_rooms: true, create_rates: true, create_tax_rates: true)
        @rt = @hotel.roomtypes[0]
        @room = @hotel.rooms[0]
        @extra = FactoryGirl.create(:extra, hotel: @hotel, reservation: false)
        @res_extra = FactoryGirl.create(:extra, hotel: @hotel, reservation: true)        
        @occupancy = FactoryGirl.build(:occupancy, room_id: @room.id, hotel: @hotel, date: Date.today)
        @occupancy2 = FactoryGirl.build(:occupancy, room_id: @room.id, hotel: @hotel, date: Date.today + 1)
        @reservation = FactoryGirl.create(:reservation, occupancies: [@occupancy, @occupancy2], hotel: @hotel)               
      end
     
  
    ## Update methods ##
    
    describe '.process_update' do
      
      it "should return the invalid request response if the request is nil" do
        @request = ReservationRequest.new()
        result = @request.process_update
        expect(result[:success]).to eq(false)
        expect(result[:errors][0][:code]).to eq("invalid_request")
      end
      
      it "should correctly delete occupancies" do
        delete_occupancies = [@occupancy.id]
        @request = ReservationRequest.new(delete_occupancies: delete_occupancies)
        @request.reservation = @reservation
        result = @request.process_update
        expect(result).to eq(true)
        expect(@reservation.occupancies.length).to eq(1)
        expect(@reservation.occupancies[0].id).to eq(@occupancy2.id)
      end
      
      it "should correctly create occupancies" do
        create_occupancies = [{date: (Date.today + 2).to_s, room_id: @room.id, guests: 1, extra_ids: [@extra.id]}]
        @request = ReservationRequest.new(create_occupancies: create_occupancies)
        @request.reservation = @reservation
        result = @request.process_update
        expect(result).to eq(true)
        expect(@reservation.occupancies.length).to eq(3)
        expect(@reservation.occupancies[2].date).to eq(Date.today + 2)
        expect(@reservation.occupancies[2].extra_ids).to eq([@extra.id])
      end
      
      it "should correctly create extras" do
        create_extras = [@res_extra.id]
        @request = ReservationRequest.new(create_extras: create_extras)
        @request.reservation = @reservation
        result = @request.process_update
        expect(result).to eq(true)
        expect(@reservation.extra_ids).to eq([@res_extra.id])
      end
      
      it "should correctly delete extras" do
        @reservation.extra_ids = [@res_extra.id]
        delete_extras = [@res_extra.id]
        @request = ReservationRequest.new(delete_extras: delete_extras)
        @request.reservation = @reservation
        result = @request.process_update
        expect(result).to eq(true)
        expect(@reservation.extra_ids).to eq([])        
      end
      
      it "should correctly modify occupancies" do
        update_occupancies = [{id: @occupancy2.id, guests: 2, extras: [@extra.id]}]
        @request = ReservationRequest.new(update_occupancies: update_occupancies)
        @request.reservation = @reservation
        result = @request.process_update
        expect(result).to eq(true)
        expect(@reservation.occupancies[1].guests).to eq(2)
        expect(@reservation.occupancies[1].extra_ids).to eq([@extra.id])                     
      end  
    end
    
        
    ## Deposit and payment methods 
    
    describe '.charge_deposit' do
    
    
    ##   
      
      before(:each) do
          @test = ReservationRequest.new()
          @hotel = Hotel.new
          allow(@hotel).to receive(:cc_override) {"override"}
          allow(@hotel).to receive(:cc_store) {false}
          allow(@hotel).to receive(:name) {"Test Hotel"}
          allow(@hotel).to receive(:uuid) {"h23a3cd3"}
          
          allow(@test).to receive(:hotel) {@hotel}
          allow(@test).to receive(:received_cc_override) {nil}
          
          @guest = FactoryGirl.build(:guest)
          allow(@test).to receive(:guest) {@guest}
          

          @reservation = Reservation.new({}, nil)
          allow(@reservation).to receive(:invoice_with_deposit) {{deposit_cents: 1234}}
          allow(@test).to receive(:reservation) {@reservation}
       end 
       
       it "should return {success: true} if the hotel cc override matches what was received" do
          allow(@test).to receive(:received_cc_override) {"override"}        
         result = @test.charge_deposit
         expect(result[:success]).to be true
       end
       
       
       it "should call self.payment.authorize_and_capture with the correct parameters" do
         
       end
       
       it "shoudl return the result of self.payment.authorize_and_capture" do
         
       end       
    end
    


end