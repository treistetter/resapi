require 'rails_helper'

describe UpdateReservation do
   before(:each) do
     @hotel = FactoryGirl.create(:hotel, create_roomtypes: true, create_rooms: true, create_rates: true)
     @rt = @hotel.roomtypes[0]
     @room = @hotel.rooms[0]
     @extra = FactoryGirl.create(:extra, hotel: @hotel, reservation: false)
     @res_extra = FactoryGirl.create(:extra, hotel: @hotel, reservation: true)        
     @res_extra2 = FactoryGirl.create(:extra, hotel: @hotel, reservation: true, price_cents: 1211)
     @occupancy = FactoryGirl.build(:occupancy, room_id: @room.id, hotel: @hotel, date: Date.today)
     @occupancy2 = FactoryGirl.build(:occupancy, room_id: @room.id, hotel: @hotel, date: Date.today + 1)
     @reservation = FactoryGirl.create(:reservation, occupancies: [@occupancy, @occupancy2], hotel: @hotel, extra_ids: [@res_extra.id])
     @create_occupancies = [{date: (Date.today + 2).to_s, room_id: @room.id, guests: 1, extra_ids: [@extra.id]}]
     @delete_occupancies = [@occupancy2.id]
     @update_occupancies = [{id: @occupancy.id, guests: 2}]
     @delete_extras = [@res_extra.id]     
     @create_extras = [@res_extra2.id]
     @request = UpdateReservation.new({create_occupancies: @create_occupancies,
                                      delete_occupancies: @delete_occupancies,
                                      update_occupancies: @update_occupancies,
                                           delete_extras: @delete_extras,
                                           create_extras: @create_extras      
       })
     @request.reservation = @reservation  
   end
   
   it "should return the correct updated occupancies" do
       result = @request.process
       expect(result[:message][:invoice][:occupancy][:occupancies][0][:date]).to eq(@occupancy.date.to_s)
       expect(result[:message][:invoice][:occupancy][:occupancies][0][:guests]).to eq(2)   
       expect(result[:message][:invoice][:occupancy][:occupancies][0][:room]).to eq(@occupancy.room_id)
       expect(result[:message][:invoice][:occupancy][:occupancies][1][:date]).to eq((Date.today + 2).to_s)
       expect(result[:message][:invoice][:occupancy][:occupancies][1][:guests]).to eq(1)   
       expect(result[:message][:invoice][:occupancy][:occupancies][1][:room]).to eq(@room.id)
   end
   
   it "should return the new invoice" do
      result = @request.process
      expect(result[:message][:invoice]).to eq(@reservation.invoice_with_payments  )
   end

   
   it "should persist the changes" do
       result = @request.process
       reservation = @hotel.find_reservation(@reservation.id)
       expect(reservation.occupancies[0].id).to eq(@occupancy.id)
       expect(reservation.occupancies[1].date).to eq(Date.today + 2)
       expect(reservation.occupancies[1].guests).to eq(1)
       expect(reservation.occupancies[1].room_id).to eq(@room.id)
       expect(reservation.occupancies[1].extra_ids).to eq([@extra.id])
       expect(reservation.occupancies.length).to eq(2)       
       expect(reservation.extra_ids).to eq([@res_extra2.id])    
         
   end
end
