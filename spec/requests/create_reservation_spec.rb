require 'rails_helper'

describe CreateReservation do
   before(:each) do
     @hotel = FactoryGirl.create(:hotel, create_roomtypes: true, create_rooms: true, create_rates: true)
     @rt = @hotel.roomtypes[0]
     @room = @hotel.rooms[0]
     
     @extra = FactoryGirl.build(:extra, hotel: @hotel, reservation: false)
     @hotel.create_extra(@extra.details)
     @res_extra = FactoryGirl.build(:extra, hotel: @hotel, reservation: true)
     @hotel.create_extra(@res_extra.details)
     
     @occ1 = {room_id: @room.id, date: Date.today + 1, guests: 1, extra_ids: [@extra.id]}
     @occ2 = {room_id: @room.id, date: Date.today + 2, guests: 1, extra_ids: [@extra.id]}
     @occ3 = {room_id: @room.id, date: Date.today + 3, guests: 1, extra_ids: [@extra.id]}         
     @occupancies = [@occ1, @occ2, @occ3]
     @extras = [@res_extra.id] 
     
     @request_params = {occupancies: @occupancies,
                             extras: @extras
                       }

     @request = CreateReservation.new(@request_params)
     @request.hotel = @hotel
   end
   
   describe '.process' do

      it 'should return the valid response given valid parameters' do
         result = @request.process
         expect(result[:success]).to eq(true)
      end


      it 'should return the failed response method if the reservation is invalid' do
         @occ2[:room_id] = "invalid"
         @occupancies = [@occ1, @occ2, @occ3]
         @request_params[:occupancies] = @occupancies
         @request = CreateReservation.new(@request_params)
         @request.user = @user
         @request.hotel = @hotel
         result = @request.process
         expect(result[:success]).to eq(false)
         expect(result[:errors]).to_not be_nil
      end    
   end
end   
     