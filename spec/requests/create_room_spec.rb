require 'rails_helper'

describe CreateRoom do
   before(:each) do
     
     @hotel = FactoryGirl.create(:hotel, create_roomtypes: true)
     @rt = @hotel.roomtypes[0]
     @new_room_hash = { name: "Test Room",
                 roomtype_id: @rt.id}
     @request = CreateRoom.new(new_room: @new_room_hash)
     @request.user = @user
     @request.hotel = @hotel
   end
   
   describe '.process' do

      it 'should return the valid response given valid parameters' do
         result = @request.process
         expect(result[:success]).to eq(true)
         expect(result[:message]).to eq(@request.new_item.details)
      end
    
      it 'should return the failed response method if the new room is invalid' do
         @new_room_hash[:name] = nil
         @request = CreateRoom.new({new_room: @new_room_hash})
         @request.user = @user
         @request.hotel = @hotel
         result = @request.process
         expect(result[:success]).to eq(false)
         expect(result[:errors]).to_not be_nil
      end    
      
      it 'should persist the new roomtype' do
         result = @request.process
         room = @hotel.db.get(result[:message][:id])
         expect(room).to_not be_nil
      end
   end
end   
     