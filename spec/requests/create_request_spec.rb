require 'rails_helper'

describe CreateRequest do
   before(:each) do
     @user = FactoryGirl.create(:user)
     @hotel = FactoryGirl.create(:hotel)  
     @request = CreateRequest.new(request: {})
     @request.model = CouchModel
     @request.description = "Couch Model"
     @request.user = @user
     @request.hotel = @hotel
     @item = CouchModel.new({}, @hotel)
     allow(@request).to receive(:new_item) {@item}
   end
   
   describe '.process' do


      it 'should return the invalid request response if validate_request returns false' do
         allow(@request).to receive(:validate_request) {false}
         allow(@request).to receive(:invalid_request_response) {"invalid response"}
         result = @request.process
         expect(result).to eq("invalid response")
      end
      
      it 'should save the new item if the request is valid' do
         allow(@request).to receive(:validate_request) {true}
         expect(@item).to receive(:save)
         @request.process        
      end
      
      it 'should return the valid response method if the new user saves successfully' do
         allow(@request).to receive(:validate_request) {true}
         allow(@request).to receive(:valid_response) {"valid response"}
         allow(@item).to receive(:save) { true }
         result = @request.process
         expect(result).to eq("valid response")         
      end
      
      it 'should return the failed response method if the new user fails to save' do
         allow(@request).to receive(:validate_request) {true}
         allow(@request).to receive(:failed_response) {"failed response"}
         allow(@item).to receive(:save) { false }
         result = @request.process
         expect(result).to eq("failed response")             
      end
      

      
   end
   
   describe '.valid_response' do
     it 'should return a hash with success true' do
        expect(@request.valid_response[:success]).to eq(true)
     end
     
     it 'should return a hash with the new item details as the message' do
        allow(@item).to receive(:details) {"details"}
        expect(@request.valid_response[:message]).to eq("details")
     end
   end

   describe '.validate_request' do
     
     it 'should create the new item object' do
         @request.validate_request
         expect(@request.new_item).to_not be_nil
         expect(@request.new_item.class).to eq(CouchModel)
     end
     
     it 'should return true if the new user is valid' do
        expect(@request.validate_request).to eq(true)
     end
     
     
     it 'should return false if the new user is not valid' do
         @request = CreateUser.new({new_user: {}})
         @request.user = @user
         @request.hotel = @hotel        
         expect(@request.validate_request).to eq(false)       
     end
   end
end   
     