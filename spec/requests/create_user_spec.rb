require 'rails_helper'

describe CreateUser do
   before(:each) do
     @user = FactoryGirl.create(:user)
     @hotel = FactoryGirl.create(:hotel)
     @new_user_hash = { first_name: "Test",
                    last_name: "User",
                        email: "email#{rand(1000)}@test#{rand(1000)}.com",
                     username: "user#{rand(1000)}",   
                         type: 'user',
                     password: 'password',
                     hotel_id: @hotel.id
       } 
     @new_user = User.new(@new_user_hash, @hotel)  
     @request = CreateUser.new({new_user: @new_user_hash})
     @request.user = @user
     @request.hotel = @hotel
   end
   
   describe '.process' do

      it 'should return the valid response given valid parameters' do
         result = @request.process
         expect(result[:success]).to eq(true)
         expect(result[:message]).to eq(@new_user.details)
      end


      
      it 'should return the failed response method if the new user is invalid' do
         @new_user_hash[:email] = nil
         @request = CreateUser.new({new_user: @new_user_hash})
         @request.user = @user
         @request.hotel = @hotel
         result = @request.process
         expect(result[:success]).to eq(false)
         expect(result[:errors]).to_not be_nil
      end    
   end
end   
     