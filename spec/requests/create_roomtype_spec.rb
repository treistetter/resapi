require 'rails_helper'

describe CreateRoomtype do
   before(:each) do
     @user = FactoryGirl.create(:user)
     @hotel = FactoryGirl.create(:hotel)
     @new_roomtype_hash = { name: "Test",
                      max_guests: 1}
     @request = CreateRoomtype.new(new_roomtype: @new_roomtype_hash)
     @request.user = @user
     @request.hotel = @hotel
   end
   
   describe '.process' do

      it 'should return the valid response given valid parameters' do
         result = @request.process
         expect(result[:success]).to eq(true)
         expect(result[:message]).to eq(@request.new_item.details)
      end
    
      it 'should return the failed response method if the new roomtype is invalid' do
         @new_roomtype_hash[:name] = nil
         @request = CreateRoomtype.new(new_roomtype: @new_roomtype_hash)
         @request.user = @user
         @request.hotel = @hotel
         result = @request.process
         expect(result[:success]).to eq(false)
         expect(result[:errors]).to_not be_nil
      end    
      
      it 'should persist the new roomtype' do
         result = @request.process
         hotel = Hotel.find_by_id(@hotel.id)
         roomtype = hotel.find_roomtype(result[:message][:id])
         expect(roomtype).to_not be_nil
      end
   end
end   
     