require 'rails_helper'

describe CreateExtra do
   before(:each) do

     @hotel = FactoryGirl.create(:hotel)
     @user = FactoryGirl.create(:user, hotel_id: @hotel.id, hotel: @hotel)
     @tr  = FactoryGirl.create(:tax_rate, hotel: @hotel)
     @tr2 = FactoryGirl.create(:tax_rate, hotel: @hotel)

     @new_extra_hash = { description: "Test Extra test test test",
                               label: "Test Extra",
                         price_cents: 1700,
                              active: true,
                         reservation: false,
                         tax_rate_ids: [@tr.id, @tr2.id],
                                room: true
                                 
       } 
     @request = CreateExtra.new(new_extra: @new_extra_hash)
     @request.hotel = @hotel
     @request.user = @user 
   end
   
   describe '.process' do

      it 'should return the valid response given valid parameters' do
         result = @request.process
         expect(result[:success]).to eq(true)
         expect(result[:message].except(:id, :pricing)).to eq(@new_extra_hash)
      end


      it 'should return the failed response method if the new extra is invalid' do
         @new_extra_hash[:label] = nil
         @request = CreateExtra.new({new_extra: @new_extra_hash})
         @request.user = @user
         @request.hotel = @hotel
         result = @request.process
         expect(result[:success]).to eq(false)
         expect(result[:errors]).to_not be_nil
      end    
   end
end   
     