Rails.application.routes.draw do
  #devise_for :users
  resources :token_authentications, :only => [:create, :destroy]

  
  root to: "front_desk#index", constraints: {subdomain: "frontdesk"}
  

  namespace 'v1' do 
      post "/login" => "sessions#login"
  end
  
  scope ':hotel_uuid' do
     namespace 'v1', constraints: { subdomain: ["api", "frontdesk"]} do  
       post "/reservations/:id/finalize" => "reservations#finalize" 
       put "/reservations/:id/finalize" => "reservations#finalize"   
       resources :roomtypes
       resources :reservations   
       resources :rooms
       get "/reservations/update/:id" => "reservations#get_update"
     end
   end   
end
